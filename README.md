
**This program take data from [Azgaar's Fantasy Map Generator](https://azgaar.github.io/Fantasy-Map-Generator/) burgs and generate items for each of them based on population**

---

## Getting burgs csv

Program works best on burgs data generated in Azgaar's Fantasy Map Generator. To get csv you need to follow these steps:

1. Open [Azgaar's Fantasy Map Generator](https://azgaar.github.io/Fantasy-Map-Generator/)
2. Generate new world or load existing one
3. Change tab to **"Tools"**
4. Click **"Burgs"** under **"Click to overview"**
5. On bottom of a window that appears find download button (fifth one)
6. Place generated csv file in same folder as **MagicItems.py**