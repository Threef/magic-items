import os
import csv
import glob
import random

verbose = False

itemList = []

def d( dice, count = 1 ):
    r = 0
    for _ in range( count ):
        r = r + 1 + random.randrange( dice )
    return r

def print_debug( *args ):
    if verbose:
        print( *args )

def createItem( item, Category ):
    item['Name'] = ""
    item['City'] = city['Burg']
    item['Province'] = city['Province']
    item['Min Price'] = city['Base Value']
    item['Category'] = Category

    item['Type'] = ""
    roll = d(100)
    if Category == "Minor":        
        if roll <= 4:
            item['Type'] = "Armor and shields"
        elif roll <=9:
            item['Type'] = "Weapons"
        elif roll <=44:
            item['Type'] = "Potions"
        elif roll <=46:
            item['Type'] = "Rings"
        elif roll <=81:
            item['Type'] = "Scrolls"
        elif roll <=91:
            item['Type'] = "Wands"
        else:
            item['Type'] = "Wonderous items"
        
    if Category == "Medium":        
        if roll <= 10:
            item['Type'] = "Armor and shields"
        elif roll <=20:
            item['Type'] = "Weapons"
        elif roll <=30:
            item['Type'] = "Potions"
        elif roll <=40:
            item['Type'] = "Rings"
        elif roll <=50:
            item['Type'] = "Rods"
        elif roll <=65:
            item['Type'] = "Scrolls"
        elif roll <=68:
            item['Type'] = "Staves"
        elif roll <=83:
            item['Type'] = "Wands"
        else:
            item['Type'] = "Wonderous items"

    if Category == "Major":        
        if roll <= 10:
            item['Type'] = "Armor and shields"
        elif roll <=20:
            item['Type'] = "Weapons"
        elif roll <=25:
            item['Type'] = "Potions"
        elif roll <=35:
            item['Type'] = "Rings"
        elif roll <=45:
            item['Type'] = "Rods"
        elif roll <=55:
            item['Type'] = "Scrolls"
        elif roll <=75:
            item['Type'] = "Staves"
        elif roll <=80:
            item['Type'] = "Wands"
        else:
            item['Type'] = "Wonderous items"

    while True:
        item['Price'] = 0
        item['Special Material'] = "No"
        item['Aura'] = []
        item['Slot'] = "No"
        item['Size'] = "Any"
        item['Weight'] = 0
        item['CL'] = 0
        
        if item['Type'] == "Armor and shields":
                createArmor( item )

        if item['Type'] == "Wonderous items":
                createWondrous( item )

        if item['Price'] >= item['Min Price']:
            break
        else:
            print_debug( "rerolling item because price too low" )

# Armor and shield
# Page 462
def createArmor( item ):

    roll = d(100)
    if roll >= 91:
        item['Size'] = "Any"
    elif roll >= 31:
        item['Size'] = "Medium"
    else:
        item['Size'] = "Small"

    roll = d(100)
    if roll >= 96:
        item['Special Material'] = random.choices( [ "Adamantine", "Darkwood", "Dragonhide", "Cold Iron", "Mithral", "Alchemical Silver" ] )

    level = 0
    levelPrice = [ 1000, 4000, 9000, 16000, 25000, 36000, 49000, 64000, 81000, 100000 ]
    special = 0

    reroll = 1
    while reroll > 0:
        roll = d(100)
        shield = False # Set this value to True if generating shield instead of armor
        if item['Category'] == "Minor":
            if 1 <= roll <= 60:
                shield = True
                level = level + 1
                item['Name'] = "+1 shield"

            if  61 <= roll <= 80:
                level = level + 1
                item['Name'] = "+1 armor"

            if  81 <= roll <= 85:
                shield = True
                level = level + 2
                item['Name'] = "+2 shield"

            if  86 <= roll <= 87:
                level = level + 2
                item['Name'] = "+2 armor"

            if  88 <= roll <= 89:
                # Specyfic armor
                roll = d(100)
                if 1 <= roll <= 50:
                    item['Name'] = "Mithral shirt"
                    item['Price'] = item['Price'] + 1100

                elif 51 <= roll <= 80:
                    item['Name'] = "Dragonhide plate"
                    item['Price'] = item['Price'] + 3300

                if 81 <= roll <= 100:
                    item['Name'] = "Elven chain"
                    item['Price'] = item['Price'] + 5150

            if  90 <= roll <= 91:
                # Specyfic shield            
                shield = True
                if 1 <= roll <= 30:
                    item['Name'] = "Darkwood buckler"
                    item['Price'] = item['Price'] + 203

                if 31 <= roll <= 80:
                    item['Name'] = "Darkwood shield"
                    item['Price'] = item['Price'] + 257

                if 81 <= roll <= 95:
                    item['Name'] = "Mithral heavy shield"
                    item['Price'] = item['Price'] + 1020
                
                if 96 <= roll <= 100:
                    item['Name'] = "Caster's shield"
                    item['Price'] = item['Price'] + 3153

            if  92 <= roll <= 100:
                # Special ability and roll again                
                print_debug( "rerolling type" )
                reroll = reroll + 1
                special = special + 1
            
        if item['Category'] == "Medium":
            if 1 <= roll <= 5:
                shield = True
                level = level + 1
                item['Name'] = "+1 shield"

            if  6 <= roll <= 10:
                level = level + 1
                item['Name'] = "+1 armor"

            if  11 <= roll <= 20:
                shield = True
                level = level + 2
                item['Name'] = "+2 shield"

            if  21 <= roll <= 30:
                level = level + 2
                item['Name'] = "+2 armor"

            if  31 <= roll <= 40:
                shield = True
                level = level + 3
                item['Name'] = "+3 shield"

            if  41 <= roll <= 50:
                level = level + 3
                item['Name'] = "+3 armor"
            
            if  51 <= roll <= 55:
                shield = True
                level = level + 4
                item['Name'] = "+4 shield"

            if  56 <= roll <= 67:
                level = level + 4
                item['Name'] = "+4 armor"

            if  58 <= roll <= 60:
                # Specyfic armor
                roll = d(100)
                if 1 <= roll <= 25:
                    item['Name'] = "Mithral shirt"
                    item['Price'] = item['Price'] + 1100

                elif 26 <= roll <= 45:
                    item['Name'] = "Dragonhide plate"
                    item['Price'] = item['Price'] + 3300

                if 46 <= roll <= 57:
                    item['Name'] = "Elven chain"
                    item['Price'] = item['Price'] + 5150

                if 58 <= roll <= 67:
                    item['Name'] = "Rhino hide"
                    item['Price'] = item['Price'] + 5165

                if 68 <= roll <= 82:
                    item['Name'] = "Adamantine breastplate"
                    item['Price'] = item['Price'] + 10200

                if 83 <= roll <= 97:
                    item['Name'] = "Dwarven plate"
                    item['Price'] = item['Price'] + 16500

                if 98 <= roll <= 100:
                    item['Name'] = "Banded mail of luck"
                    item['Price'] = item['Price'] + 18900

            if  61 <= roll <= 63:
                # Specyfic shield            
                shield = True
                if 1 <= roll <= 20:
                    item['Name'] = "Darkwood buckler"
                    item['Price'] = item['Price'] + 203

                if 21 <= roll <= 45:
                    item['Name'] = "Darkwood shield"
                    item['Price'] = item['Price'] + 257

                if 46 <= roll <= 70:
                    item['Name'] = "Mithral heavy shield"
                    item['Price'] = item['Price'] + 1020
                
                if 71 <= roll <= 85:
                    item['Name'] = "Caster's shield"
                    item['Price'] = item['Price'] + 3153

                if 86 <= roll <= 90:
                    item['Name'] = "Spined shield"
                    item['Price'] = item['Price'] + 5580

                if 91 <= roll <= 95:
                    item['Name'] = "Lion's shield"
                    item['Price'] = item['Price'] + 9170

                if 96 <= roll <= 100:
                    item['Name'] = "Winged shield"
                    item['Price'] = item['Price'] + 17257

            if  64 <= roll <= 100:
                # Special ability and roll again                
                print_debug( "rerolling type" )
                reroll = reroll + 1
                special = special + 1

        if item['Category'] == "Major":
            if 1 <= roll <= 8:
                shield = True
                level = level + 3
                item['Name'] = "+3 shield"

            if 9 <= roll <= 16:
                level = level + 3
                item['Name'] = "+3 armor"

            if 17 <= roll <= 27:                    
                shield = True
                level = level + 4
                item['Name'] = "+4 shield"

            if 28 <= roll <= 38:
                level = level + 4
                item['Name'] = "+4 armor"

            if 39 <= roll <= 49:                    
                shield = True
                level = level + 5
                item['Name'] = "+5 shield"

            if 50 <= roll <= 57:
                level = level + 5
                item['Name'] = "+5 armor"

            if  58 <= roll <= 60:
                # Specyfic armor
                roll = d(100)
                if 1 <= roll <= 10:
                    item['Name'] = "Adamantine breastplate"
                    item['Price'] = item['Price'] + 10200

                if 11 <= roll <= 20:
                    item['Name'] = "Dwarven plate"
                    item['Price'] = item['Price'] + 16500

                if 21 <= roll <= 32:
                    item['Name'] = "Banded mail of luck"
                    item['Price'] = item['Price'] + 18900

                if 33 <= roll <= 50:
                    item['Name'] = "Celestial armor"
                    item['Price'] = item['Price'] + 22400

                if 51 <= roll <= 60:
                    item['Name'] = "Plate Armor of the deep"
                    item['Price'] = item['Price'] + 24650

                if 61 <= roll <= 75:
                    item['Name'] = "Breastplate of command"
                    item['Price'] = item['Price'] + 25400

                if 76 <= roll <= 90:
                    item['Name'] = "Mitral full plate of speed"
                    item['Price'] = item['Price'] + 26500

                if 91 <= roll <= 100:
                    item['Name'] = "Demon armor"
                    item['Price'] = item['Price'] + 52260

            if  61 <= roll <= 63:
                # Specyfic shield            
                shield = True
                if 1 <= roll <= 20:
                    item['Name'] = "Caster's shield"
                    item['Price'] = item['Price'] + 3153

                if 21 <= roll <= 40:
                    item['Name'] = "Spined shield"
                    item['Price'] = item['Price'] + 5580

                if 41 <= roll <= 60:
                    item['Name'] = "Lion's shield"
                    item['Price'] = item['Price'] + 9170

                if 61 <= roll <= 90:
                    item['Name'] = "Winged shield"
                    item['Price'] = item['Price'] + 17257

                if 91 <= roll <= 10:
                    item['Name'] = "Absorbing shield"
                    item['Price'] = item['Price'] + 50170

            if  64 <= roll <= 100:
                # Special ability and roll again                
                print_debug( "rerolling type" )
                reroll = reroll + 1
                special = special + 1

        reroll = reroll - 1

    i = 0
    while special > 0:
        i = i + 1
        item['Name'] = item['Name'] + ", "

        roll = d(100)
        if shield == False:
            if item['Category'] == "Minor":
                if 1 <= roll <= 25:
                    item['Name'] = item['Name'] + "Glamered"
                    item['Price'] = item['Price'] + 2700
                    item['Aura'].append( "Moderate illusion" )
                    item['CL'] = max ( item['CL'], 10)

                elif 26 <= roll <= 32:
                    item['Name'] = item['Name'] + "Light Fortification"
                    level = level + 1
                    item['Aura'].append( "Strong abjuration" )
                    item['CL'] = max ( item['CL'], 13)

                elif 33 <= roll <= 52:
                    item['Name'] = item['Name'] + "Slick"
                    item['Price'] = item['Price'] + 3750
                    item['Aura'].append( "Faint conjuration" )
                    item['CL'] = max ( item['CL'], 4)

                elif 53 <= roll <= 92:
                    item['Name'] = item['Name'] + "Shadow"
                    item['Price'] = item['Price'] + 3750
                    item['Aura'].append( "Faint illusion" )
                    item['CL'] = max ( item['CL'], 5)

                elif 93 <= roll <= 96:
                    item['Name'] = item['Name'] + "Spell Resistance (13)"
                    level = level + 2
                    item['Aura'].append( "Strong abjuration" )
                    item['CL'] = max ( item['CL'], 15)

                elif roll == 97:
                    item['Name'] = item['Name'] + "Improved Slick"
                    item['Price'] = item['Price'] + 15000
                    item['Aura'].append( "Moderate conjuration" )
                    item['CL'] = max ( item['CL'], 10)

                elif 98 <= roll <= 99:
                    item['Name'] = item['Name'] + "Improved Shadow"
                    item['Price'] = item['Price'] + 15000
                    item['Aura'].append( "Moderate illusion" )
                    item['CL'] = max ( item['CL'], 10)

                else:
                    special = special + 2
                    print_debug( "rerolling specials" )

            if item['Category'] == "Medium":
                if 1 <= roll <= 5:
                    item['Name'] = item['Name'] + "Glamered"
                    item['Price'] = item['Price'] + 2700
                    item['Aura'].append( "Moderate illusion" )
                    item['CL'] = max ( item['CL'], 10)

                elif 6 <= roll <= 8:
                    item['Name'] = item['Name'] + "Light Fortification"
                    level = level + 1
                    item['Aura'].append( "Strong abjuration" )
                    item['CL'] = max ( item['CL'], 13)

                elif 9 <= roll <= 1:
                    item['Name'] = item['Name'] + "Slick"
                    item['Price'] = item['Price'] + 3750
                    item['Aura'].append( "Faint conjuration" )
                    item['CL'] = max ( item['CL'], 4)

                elif 12 <= roll <= 17:
                    item['Name'] = item['Name'] + "Shadow"
                    item['Price'] = item['Price'] + 3750
                    item['Aura'].append( "Faint illusion" )
                    item['CL'] = max ( item['CL'], 5)

                elif 18 <= roll <= 19:
                    item['Name'] = item['Name'] + "Spell Resistance (13)"
                    level = level + 2
                    item['Aura'].append( "Strong abjuration" )
                    item['CL'] = max ( item['CL'], 15)

                elif 20 <= roll <= 29:
                    item['Name'] = item['Name'] + "Improved Slick"
                    item['Price'] = item['Price'] + 15000
                    item['Aura'].append( "Moderate conjuration" )
                    item['CL'] = max ( item['CL'], 10)

                elif 30 <= roll <= 49:
                    item['Name'] = item['Name'] + "Improved Shadow"
                    item['Price'] = item['Price'] + 15000
                    item['Aura'].append( "Moderate illusion" )
                    item['CL'] = max ( item['CL'], 10)

                elif 50 <= roll <= 74:
                    item['Name'] = item['Name'] + "Energy resistance"
                    item['Price'] = item['Price'] + 18000
                    item['Aura'].append( "Faint abjuration" )
                    item['CL'] = max ( item['CL'], 3)

                elif 75 <= roll <= 79:
                    item['Name'] = item['Name'] + "Ghost touch"
                    level = level + 3
                    item['Aura'].append( "Strong transmutation" )
                    item['CL'] = max ( item['CL'], 15)
                
                elif 80 <= roll <= 84:
                    item['Name'] = item['Name'] + "Invulnerability"
                    level = level + 3
                    item['Aura'].append( "Strong abjuration" )
                    item['CL'] = max ( item['CL'], 18)
                
                elif 85 <= roll <= 89:
                    item['Name'] = item['Name'] + "Moderate Fortification"
                    level = level + 3
                    item['Aura'].append( "Strong abjuration" )
                    item['CL'] = max ( item['CL'], 13)

                elif 90 <= roll <= 94:
                    item['Name'] = item['Name'] + "Spell Resistance (15)"
                    level = level + 3
                    item['Aura'].append( "Strong abjuration" )
                    item['CL'] = max ( item['CL'], 15)

                elif 95 <= roll <= 99:
                    item['Name'] = item['Name'] + "Wild"
                    level = level + 3
                    item['Aura'].append( "Moderate transmutation" )
                    item['CL'] = max ( item['CL'], 9)

                else:
                    special = special + 2
                    print_debug( "rerolling specials" )

            if item['Category'] == "Major":
                if 1 <= roll <= 3:
                    item['Name'] = item['Name'] + "Glamered"
                    item['Price'] = item['Price'] + 2700
                    item['Aura'].append( "Moderate illusion" )
                    item['CL'] = max ( item['CL'], 10)

                elif roll == 4:
                    item['Name'] = item['Name'] + "Light Fortification"
                    level = level + 1
                    item['Aura'].append( "Strong abjuration" )
                    item['CL'] = max ( item['CL'], 13)

                elif 5 <= roll <= 7:
                    item['Name'] = item['Name'] + "Improved Slick"
                    item['Price'] = item['Price'] + 15000
                    item['Aura'].append( "Moderate conjuration" )
                    item['CL'] = max ( item['CL'], 10)

                elif 8 <= roll <= 13:
                    item['Name'] = item['Name'] + "Improved Shadow"
                    item['Price'] = item['Price'] + 15000
                    item['Aura'].append( "Moderate illusion" )
                    item['CL'] = max ( item['CL'], 10)

                elif 14 <= roll <= 28:
                    item['Name'] = item['Name'] + "Energy resistance"
                    item['Price'] = item['Price'] + 18000
                    item['Aura'].append( "Faint abjuration" )
                    item['CL'] = max ( item['CL'], 3)

                elif 29 <= roll <= 33:
                    item['Name'] = item['Name'] + "Ghost touch"
                    level = level + 3
                    item['Aura'].append( "Strong transmutation" )
                    item['CL'] = max ( item['CL'], 15)
                
                elif 34 <= roll <= 35:
                    item['Name'] = item['Name'] + "Invulnerability"
                    level = level + 3
                    item['Aura'].append( "Strong abjuration" )
                    item['CL'] = max ( item['CL'], 18)
                
                elif 36 <= roll <= 40:
                    item['Name'] = item['Name'] + "Moderate Fortification"
                    level = level + 3
                    item['Aura'].append( "Strong abjuration" )
                    item['CL'] = max ( item['CL'], 13)

                elif 41 <= roll <= 42:
                    item['Name'] = item['Name'] + "Spell Resistance (15)"
                    level = level + 3
                    item['Aura'].append( "Strong abjuration" )
                    item['CL'] = max ( item['CL'], 15)

                elif roll == 43:
                    item['Name'] = item['Name'] + "Wild"
                    level = level + 3
                    item['Aura'].append( "Moderate transmutation" )
                    item['CL'] = max ( item['CL'], 9)

                elif 44 <= roll <= 48:
                    item['Name'] = item['Name'] + "Greater Slick"
                    item['Price'] = item['Price'] + 33750
                    item['Aura'].append( "Strong conjuration" )
                    item['CL'] = max ( item['CL'], 15)

                elif 49 <= roll <= 58:
                    item['Name'] = item['Name'] + "Greater Shadow"
                    item['Price'] = item['Price'] + 33750
                    item['Aura'].append( "Strong illusion" )
                    item['CL'] = max ( item['CL'], 15)

                elif 59 <= roll <= 83:
                    item['Name'] = item['Name'] + "Improved Energy resistance"
                    item['Price'] = item['Price'] + 42000
                    item['Aura'].append( "Moderate abjuration" )
                    item['CL'] = max ( item['CL'], 7)

                elif 84 <= roll <= 88:
                    item['Name'] = item['Name'] + "Spell Resistance (17)"
                    level = level + 4
                    item['Aura'].append( "Strong abjuration" )
                    item['CL'] = max ( item['CL'], 15)

                elif roll == 89:
                    item['Name'] = item['Name'] + "Etherealness"
                    item['Price'] = item['Price'] + 49000
                    item['Aura'].append( "Strong transmutation" )
                    item['CL'] = max ( item['CL'], 13)

                elif roll == 90:
                    item['Name'] = item['Name'] + "Undead controlling"
                    item['Price'] = item['Price'] + 49000
                    item['Aura'].append( "Strong necromancy" )
                    item['CL'] = max ( item['CL'], 13)

                elif 91 <= roll <= 92:
                    item['Name'] = item['Name'] + "Heavy Fortification"
                    level = level + 5
                    item['Aura'].append( "Strong abjuration" )
                    item['CL'] = max ( item['CL'], 13)

                elif 93 <= roll <= 94:
                    item['Name'] = item['Name'] + "Spell Resistance (19)"
                    level = level + 5
                    item['Aura'].append( "Strong abjuration" )
                    item['CL'] = max ( item['CL'], 15)

                elif 95 <= roll <= 99:
                    item['Name'] = item['Name'] + "Greater Energy resistance"
                    item['Price'] = item['Price'] + 66000
                    item['Aura'].append( "Moderate abjuration" )
                    item['CL'] = max ( item['CL'], 11)

                else:
                    special = special + 2
                    print_debug( "rerolling specials" )

            elif shield == True:

                if item['Category'] == "Minor":
                    if 1 <= roll <= 20:
                        item['Name'] = item['Name'] + "Arrow catching"
                        level = level + 1
                        item['Aura'].append( "Moderate abjuration" )
                        item['CL'] = max ( item['CL'], 8)

                    elif 24 <= roll <= 40:
                        item['Name'] = item['Name'] + "Bashing"
                        level = level + 1
                        item['Aura'].append( "Moderate transmutation" )
                        item['CL'] = max ( item['CL'], 8)
                    
                    elif 41 <= roll <= 50:
                        item['Name'] = item['Name'] + "Blinding"
                        level = level + 1
                        item['Aura'].append( "Moderate evocation" )
                        item['CL'] = max ( item['CL'], 7)

                    elif 51 <= roll <= 75:
                        item['Name'] = item['Name'] + "Light Fortification"
                        level = level + 1
                        item['Aura'].append( "Strong abjuration" )
                        item['CL'] = max ( item['CL'], 13)

                    elif 76 <= roll <= 92:
                        item['Name'] = item['Name'] + "Arrow deflection"
                        level = level + 2
                        item['Aura'].append( "Faint abjuration" )
                        item['CL'] = max ( item['CL'], 5)

                    elif 93 <= roll <= 97:
                        item['Name'] = item['Name'] + "Animated"
                        level = level + 2
                        item['Aura'].append( "Strong transmutation" )
                        item['CL'] = max ( item['CL'], 12)
                    
                    elif 98 <= roll <= 99:
                        item['Name'] = item['Name'] + "Spell Resistance (15)"
                        level = level + 3
                        item['Aura'].append( "Strong abjuration" )
                        item['CL'] = max ( item['CL'], 15)

                    else:
                        special = special + 2
                        print_debug( "rerolling specials" )
                    
                if item['Category'] == "Medium":
                    if 1 <= roll <= 10:
                        item['Name'] = item['Name'] + "Arrow catching"
                        level = level + 1
                        item['Aura'].append( "Moderate abjuration" )
                        item['CL'] = max ( item['CL'], 8)

                    elif 11 <= roll <= 20:
                        item['Name'] = item['Name'] + "Bashing"
                        level = level + 1
                        item['Aura'].append( "Moderate transmutation" )
                        item['CL'] = max ( item['CL'], 8)
                    
                    elif 21 <= roll <= 25:
                        item['Name'] = item['Name'] + "Blinding"
                        level = level + 1
                        item['Aura'].append( "Moderate evocation" )
                        item['CL'] = max ( item['CL'], 7)

                    elif 26 <= roll <= 40:
                        item['Name'] = item['Name'] + "Light Fortification"
                        level = level + 1
                        item['Aura'].append( "Strong abjuration" )
                        item['CL'] = max ( item['CL'], 13)

                    elif 41 <= roll <= 50:
                        item['Name'] = item['Name'] + "Arrow deflection"
                        level = level + 2
                        item['Aura'].append( "Faint abjuration" )
                        item['CL'] = max ( item['CL'], 5)

                    elif 51 <= roll <= 57:
                        item['Name'] = item['Name'] + "Animated"
                        level = level + 2
                        item['Aura'].append( "Strong transmutation" )
                        item['CL'] = max ( item['CL'], 12)
                    
                    elif 58 <= roll <= 59:
                        item['Name'] = item['Name'] + "Spell Resistance (15)"
                        level = level + 3
                        item['Aura'].append( "Strong abjuration" )
                        item['CL'] = max ( item['CL'], 15)

                    elif 60 <= roll <= 79:
                        item['Name'] = item['Name'] + "Energy resistance"
                        item['Price'] = item['Price'] + 18000
                        item['Aura'].append( "Faint abjuration" )
                        item['CL'] = max ( item['CL'], 3)

                    elif 80 <= roll <= 85:
                        item['Name'] = item['Name'] + "Ghost touch"
                        level = level + 3
                        item['Aura'].append( "Strong transmutation" )
                        item['CL'] = max ( item['CL'], 15)
                    
                    elif 86 <= roll <= 95:
                        item['Name'] = item['Name'] + "Moderate Fortification"
                        level = level + 3
                        item['Aura'].append( "Strong abjuration" )
                        item['CL'] = max ( item['CL'], 13)

                    elif 96 <= roll <= 98:
                        item['Name'] = item['Name'] + "Spell Resistance (15)"
                        level = level + 3
                        item['Aura'].append( "Strong abjuration" )
                        item['CL'] = max ( item['CL'], 15)

                    elif roll == 99:
                        item['Name'] = item['Name'] + "Wild"
                        level = level + 3
                        item['Aura'].append( "Moderate transmutation" )
                        item['CL'] = max ( item['CL'], 9)

                    else:
                        special = special + 2
                        print_debug( "rerolling specials" )

                if item['Category'] == "Major":
                    if 1 <= roll <= 5:
                        item['Name'] = item['Name'] + "Arrow catching"
                        level = level + 1
                        item['Aura'].append( "Moderate abjuration" )
                        item['CL'] = max ( item['CL'], 8)

                    elif 6 <= roll <= 8:
                        item['Name'] = item['Name'] + "Bashing"
                        level = level + 1
                        item['Aura'].append( "Moderate transmutation" )
                        item['CL'] = max ( item['CL'], 8)
                    
                    elif 9 <= roll <= 10:
                        item['Name'] = item['Name'] + "Blinding"
                        level = level + 1
                        item['Aura'].append( "Moderate evocation" )
                        item['CL'] = max ( item['CL'], 7)

                    elif 11 <= roll <= 15:
                        item['Name'] = item['Name'] + "Light Fortification"
                        level = level + 1
                        item['Aura'].append( "Strong abjuration" )
                        item['CL'] = max ( item['CL'], 13)

                    elif 16 <= roll <= 20:
                        item['Name'] = item['Name'] + "Arrow deflection"
                        level = level + 2
                        item['Aura'].append( "Faint abjuration" )
                        item['CL'] = max ( item['CL'], 5)

                    elif 21 <= roll <= 25:
                        item['Name'] = item['Name'] + "Animated"
                        level = level + 2
                        item['Aura'].append( "Strong transmutation" )
                        item['CL'] = max ( item['CL'], 12)

                    elif 26 <= roll <= 41:
                        item['Name'] = item['Name'] + "Energy resistance"
                        item['Price'] = item['Price'] + 18000
                        item['Aura'].append( "Faint abjuration" )
                        item['CL'] = max ( item['CL'], 3)

                    elif 42 <= roll <= 46:
                        item['Name'] = item['Name'] + "Ghost touch"
                        level = level + 3
                        item['Aura'].append( "Strong transmutation" )
                        item['CL'] = max ( item['CL'], 15)
                    
                    elif 47 <= roll <= 56:
                        item['Name'] = item['Name'] + "Moderate Fortification"
                        level = level + 3
                        item['Aura'].append( "Strong abjuration" )
                        item['CL'] = max ( item['CL'], 13)

                    elif 57 <= roll <= 58:
                        item['Name'] = item['Name'] + "Spell Resistance (15)"
                        level = level + 3
                        item['Aura'].append( "Strong abjuration" )
                        item['CL'] = max ( item['CL'], 15)

                    elif roll == 59:
                        item['Name'] = item['Name'] + "Wild"
                        level = level + 3
                        item['Aura'].append( "Moderate transmutation" )
                        item['CL'] = max ( item['CL'], 9)

                    elif 60 <= roll <= 84:
                        item['Name'] = item['Name'] + "Improved Energy resistance"
                        item['Price'] = item['Price'] + 42000
                        item['Aura'].append( "Moderate abjuration" )
                        item['CL'] = max ( item['CL'], 7)

                    elif 85 <= roll <= 86:
                        item['Name'] = item['Name'] + "Spell Resistance (17)"
                        level = level + 4
                        item['Aura'].append( "Strong abjuration" )
                        item['CL'] = max ( item['CL'], 15)

                    elif roll == 87:
                        item['Name'] = item['Name'] + "Undead controlling"
                        item['Price'] = item['Price'] + 49000
                        item['Aura'].append( "Strong necromancy" )
                        item['CL'] = max ( item['CL'], 13)

                    elif 88 <= roll <= 91:
                        item['Name'] = item['Name'] + "Heavy Fortification"
                        level = level + 5
                        item['Aura'].append( "Strong abjuration" )
                        item['CL'] = max ( item['CL'], 13)

                    elif 92 <= roll <= 93:
                        item['Name'] = item['Name'] + "Reflecting"
                        level = level + 5
                        item['Aura'].append( "Strong abjuration" )
                        item['CL'] = max ( item['CL'], 14)

                    elif roll == 94:
                        item['Name'] = item['Name'] + "Spell Resistance (19)"
                        level = level + 5
                        item['Aura'].append( "Strong abjuration" )
                        item['CL'] = max ( item['CL'], 15)

                    elif 95 <= roll <= 99:
                        item['Name'] = item['Name'] + "Greater Energy resistance"
                        item['Price'] = item['Price'] + 66000
                        item['Aura'].append( "Moderate abjuration" )
                        item['CL'] = max ( item['CL'], 11)

                    else:
                        special = special + 2
                        print_debug( "rerolling specials" )

        special = special -1
        
    if level > 0:
        if verbose:
            print( level, item["Name"] )
        item['Price'] = item['Price'] + levelPrice[ level ]

# Wonderous items
# Page 497
def createWondrous( item ):
    roll = d(100)
    item['Inteligent?'] = False
    item['Clue?'] = False
    if roll == 1:
        item['Inteligent?'] = True
    elif roll <= 31:
        item['Clue?'] = True

    fc = "Faint conjuration"
    mc = "Moderate conjuration"
    sc = "Strong conjuration"
    fa = "Faint abjuration"
    ma = "Moderate abjuration"
    sa = "Strong abjuration"
    fn = "Faint necromancy"
    mn = "Moderate necromancy"
    sn = "Strong necromancy"
    fi = "Faint illusion"
    mi = "Moderate illusion"
    si = "Strong illusion"
    fd = "Faint divination"
    md = "Moderate divination"
    sd = "Strong divination"
    ft = "Faint transmutation"
    mt = "Moderate transmutation"
    st = "Strong transmutation"
    fe = "Faint evocation"
    me = "Moderate evocation"
    se = "Strong evocation"
    fen = "Faint enchantment"
    men = "Moderate enchantment"
    sen = "Strong enchantment"

    roll = d(100)
    if item['Category'] == "Minor":
        if roll == 1:
            item['Name'] = "Feather token, anchor"
            item['Aura'] = sc
            item['Slot'] = "None"
            item['CL'] = 12
            item['Weight'] = 0
            item['Price'] = item['Price'] + 50
        if roll == 2:
            item['Name'] = "Universal solvent"
            item['Aura'] = ft
            item['Slot'] = "None"
            item['CL'] = 3
            item['Weight'] = 0
            item['Price'] = item['Price'] + 50
        if roll == 3:
            item['Name'] = "Elixir of love"
            item['Aura'] = fen
            item['Slot'] = "None"
            item['CL'] = 4
            item['Weight'] = 1
            item['Price'] = item['Price'] + 150
        if roll == 4:
            item['Name'] = "Unguent of timelessness"
            item['Aura'] = ft
            item['Slot'] = "None"
            item['CL'] = 3
            item['Weight'] = 0
            item['Price'] = item['Price'] + 150
        if roll == 5:
            item['Name'] = "Feather token, fan"
            item['Aura'] = sc
            item['Slot'] = "None"
            item['CL'] = 12
            item['Weight'] = 0
            item['Price'] = item['Price'] + 200
        if roll == 6:
            item['Name'] = "Dust of tracelessness"
            item['Aura'] = "Faint transmutation"
            item['Slot'] = "None"
            item['CL'] = 3
            item['Weight'] = 0
            item['Price'] = item['Price'] + 250
        if roll == 7:
            item['Name'] = "Elixir of hiding"
            item['Aura'] = fi
            item['Slot'] = "None"
            item['CL'] = 5
            item['Weight'] = 0
            item['Price'] = item['Price'] + 250
        if roll == 8:
            item['Name'] = "Elixir of tumbling"
            item['Aura'] = ft
            item['Slot'] = "None"
            item['CL'] = 5
            item['Weight'] = 0
            item['Price'] = item['Price'] + 250
        if roll == 9:
            item['Name'] = "Elixir of swimming"
            item['Aura'] = ft
            item['Slot'] = "None"
            item['CL'] = 2
            item['Weight'] = 0
            item['Price'] = item['Price'] + 250
        if roll == 10:
            item['Name'] = "Elixir of vision"
            item['Aura'] = fd
            item['Slot'] = "None"
            item['CL'] = 2
            item['Weight'] = 0
            item['Price'] = item['Price'] + 250
        if roll == 11:
            item['Name'] = "Silversheen"
            item['Aura'] = ft
            item['Slot'] = "Neck"
            item['CL'] = 5
            item['Weight'] = 0
            item['Price'] = item['Price'] + 250
        if roll == 12:
            item['Name'] = "Feather token, bird"
            item['Aura'] = sc
            item['Slot'] = "None"
            item['CL'] = 12
            item['Weight'] = 0
            item['Price'] = item['Price'] + 300
        if roll == 13:
            item['Name'] = "Feather token, tree"
            item['Aura'] = sc
            item['Slot'] = "None"
            item['CL'] = 12
            item['Weight'] = 0
            item['Price'] = item['Price'] + 400
        if roll == 14:
            item['Name'] = "Feather token, swan boat"
            item['Aura'] = sc
            item['Slot'] = "None"
            item['CL'] = 12
            item['Weight'] = 0
            item['Price'] = item['Price'] + 450
        if roll == 15:
            item['Name'] = "Elixir of truth"
            item['Aura'] = fen
            item['Slot'] = "None"
            item['CL'] = 5
            item['Weight'] = 0
            item['Price'] = item['Price'] + 500
        if roll == 16:
            item['Name'] = "Feather token, whip"
            item['Aura'] = sc
            item['Slot'] = "None"
            item['CL'] = 12
            item['Weight'] = 0
            item['Price'] = item['Price'] + 500
        if roll == 17:
            item['Name'] = "Dust of dryness"
            item['Aura'] = "MOderate transmutation"
            item['Slot'] = "None"
            item['CL'] = 11
            item['Weight'] = 0
            item['Price'] = item['Price'] + 850
        if roll == 18:
            item['Name'] = "Hand of the mage"
            item['Aura'] = mc
            item['Slot'] = "Neck"
            item['CL'] = 9
            item['Weight'] = 2
            item['Price'] = item['Price'] + 900
        if roll == 19:
            item['Name'] = "Bracers of armor +1"
            item['Aura'] = "Moderate conjuration"
            item['Slot'] = "None"
            item['CL'] = 7
            item['Weight'] = 1
            item['Price'] = item['Price'] + 1000
        if roll == 20:
            item['Name'] = "Cloak of resistance +1"
            item['Aura'] = "Faint abjuration"
            item['Slot'] = "Shoulders"
            item['CL'] = 5
            item['Weight'] = 1
            item['Price'] = item['Price'] + 1000
        if roll == 21:
            item['Name'] = "Pearl of power, 1st-level spell"
            item['Aura'] = st
            item['Slot'] = "None"
            item['CL'] = 17
            item['Weight'] = 0
            item['Price'] = item['Price'] + 1000
        if roll == 22:
            item['Name'] = "Phylactery of faithfulness"
            item['Aura'] = fd
            item['Slot'] = "Headband"
            item['CL'] = 1
            item['Weight'] = 0
            item['Price'] = item['Price'] + 1000
        if roll == 23:
            item['Name'] = "Salve of slipperiness"
            item['Aura'] = mc
            item['Slot'] = "None"
            item['CL'] = 6
            item['Weight'] = 0
            item['Price'] = item['Price'] + 1000
        if roll == 24:
            item['Name'] = "Elixir of fire breath"
            item['Aura'] = me
            item['Slot'] = "None"
            item['CL'] = 11
            item['Weight'] = 0
            item['Price'] = item['Price'] + 1100
        if roll == 25:
            item['Name'] = "Pipes of the sewers"
            item['Aura'] = fc
            item['Slot'] = "None"
            item['CL'] = 2
            item['Weight'] = 3
            item['Price'] = item['Price'] + 1150
        if roll == 26:
            item['Name'] = "Dust of illusion"
            item['Aura'] = "Moderate illusion"
            item['Slot'] = "None"
            item['CL'] = 6
            item['Weight'] = 0
            item['Price'] = item['Price'] + 1200
        if roll == 27:
            item['Name'] = "Brooch of shielding"
            item['Aura'] = "Faint abjuration"
            item['Slot'] = "Neck"
            item['CL'] = 1
            item['Weight'] = 0
            item['Price'] = item['Price'] + 1500
        if roll == 28:
            item['Name'] = "Necklace of fireballs type I"
            item['Aura'] = me
            item['Slot'] = "Neck"
            item['CL'] = 10
            item['Weight'] = 1
            item['Price'] = item['Price'] + 1650
        if roll == 29:
            item['Name'] = "Dust of appearance"
            item['Aura'] = "Moderate conjuration"
            item['Slot'] = "None"
            item['CL'] = 7
            item['Weight'] = 0
            item['Price'] = item['Price'] + 1800
        if roll == 30:
            item['Name'] = "Hat of disguise"
            item['Aura'] = fi
            item['Slot'] = "Head"
            item['CL'] = 1
            item['Weight'] = 0
            item['Price'] = item['Price'] + 1800
        if roll == 31:
            item['Name'] = "Pipes of sounding"
            item['Aura'] = fi
            item['Slot'] = "None"
            item['CL'] = 2
            item['Weight'] = 3
            item['Price'] = item['Price'] + 1800
        if roll == 32:
            item['Name'] = "Efficient quiver"
            item['Aura'] = "Moderate conjuration"
            item['Slot'] = "None"
            item['CL'] = 9
            item['Weight'] = 2
            item['Price'] = item['Price'] + 1800
        if roll == 33:
            item['Name'] = "Amulet of natural armor +1"
            item['Aura'] = "Faint transmutation"
            item['Slot'] = "Neck"
            item['CL'] = 5
            item['Weight'] = 0
            item['Price'] = item['Price'] + 2000
        if roll == 34:
            item['Name'] = "Handy haversack"
            item['Aura'] = mc
            item['Slot'] = "None"
            item['CL'] = 2
            item['Weight'] = 5
            item['Price'] = item['Price'] + 2000
        if roll == 35:
            item['Name'] = "Horn of fog"
            item['Aura'] = fc
            item['Slot'] = "None"
            item['CL'] = 3
            item['Weight'] = 1
            item['Price'] = item['Price'] + 2000
        if roll == 36:
            item['Name'] = "Elemental gem"
            item['Aura'] = mc
            item['Slot'] = "None"
            item['CL'] = 11
            item['Weight'] = 0
            item['Price'] = item['Price'] + 2250
        if roll == 37:
            item['Name'] = "Robe of bones"
            item['Aura'] = "Moderate necromancy [evil]"
            item['Slot'] = "Body"
            item['CL'] = 6
            item['Weight'] = 1
            item['Price'] = item['Price'] + 2400
        if roll == 38:
            item['Name'] = "Sovereign glue"
            item['Aura'] = st
            item['Slot'] = "None"
            item['CL'] = 20
            item['Weight'] = 0
            item['Price'] = item['Price'] + 2400
        if roll == 39:
            item['Name'] = "Bag of holding type I"
            item['Aura'] = "Moderate conjuration"
            item['Slot'] = "None"
            item['CL'] = 9
            item['Weight'] = 15
            item['Price'] = item['Price'] + 2500
        if roll == 40:
            item['Name'] = "Boots of elvenkind"
            item['Aura'] = "Faint transmutation"
            item['Slot'] = "Feet"
            item['CL'] = 5
            item['Weight'] = 1
            item['Price'] = item['Price'] + 2500
        if roll == 41:
            item['Name'] = "Boots of the winterlands"
            item['Aura'] = "Faint abjuration and transmutation"
            item['Slot'] = "Feet"
            item['CL'] = 5
            item['Weight'] = 1
            item['Price'] = item['Price'] + 2500
        if roll == 42:
            item['Name'] = "Candle of truth"
            item['Aura'] = "Faint enchantment"
            item['Slot'] = "None"
            item['CL'] = 3
            item['Weight'] = 0.5
            item['Price'] = item['Price'] + 2500
        if roll == 43:
            item['Name'] = "Cloak of elvenkind"
            item['Aura'] = "Faint illusion"
            item['Slot'] = "Shoulders"
            item['CL'] = 3
            item['Weight'] = 1
            item['Price'] = item['Price'] + 2500
        if roll == 44:
            item['Name'] = "Eyes of the eagle"
            item['Aura'] = fd
            item['Slot'] = "Eyes"
            item['CL'] = 3
            item['Weight'] = 0
            item['Price'] = item['Price'] + 2500
        if roll == 45:
            item['Name'] = "Goggles of minute seeing"
            item['Aura'] = fd
            item['Slot'] = "Eyes"
            item['CL'] = 3
            item['Weight'] = 0
            item['Price'] = item['Price'] + 2500
        if roll == 46:
            item['Name'] = "Scarab, golembane"
            item['Aura'] = md
            item['Slot'] = "Neck"
            item['CL'] = 8
            item['Weight'] = 0
            item['Price'] = item['Price'] + 2500
        if roll == 47:
            item['Name'] = "Necklace of fireballs type II"
            item['Aura'] = me
            item['Slot'] = "Neck"
            item['CL'] = 10
            item['Weight'] = 1
            item['Price'] = item['Price'] + 2700
        if roll == 48:
            item['Name'] = "Stone of alarm"
            item['Aura'] = fa
            item['Slot'] = "None"
            item['CL'] = 3
            item['Weight'] = 2
            item['Price'] = item['Price'] + 2700
        if roll == 49:
            item['Name'] = "Bead of force"
            item['Aura'] = "Moderate evocation"
            item['Slot'] = "none"
            item['CL'] = 10
            item['Weight'] = 0
            item['Price'] = item['Price'] + 3000
        if roll == 50:
            item['Name'] = "Chime of opening"
            item['Aura'] = "Moderate transmutation"
            item['Slot'] = "None"
            item['CL'] = 11
            item['Weight'] = 1
            item['Price'] = item['Price'] + 3000
        if roll == 51:
            item['Name'] = "Horseshoes of speed"
            item['Aura'] = ft
            item['Slot'] = "Feet"
            item['CL'] = 3
            item['Weight'] = 12
            item['Price'] = item['Price'] + 3000
        if roll == 52:
            item['Name'] = "Rope of climbing"
            item['Aura'] = ft
            item['Slot'] = "None"
            item['CL'] = 3
            item['Weight'] = 3
            item['Price'] = item['Price'] + 3000
        if roll == 53:
            item['Name'] = "Bag of tricks, gray"
            item['Aura'] = "Faint conjuration"
            item['Slot'] = "None"
            item['CL'] = 3
            item['Weight'] = 0
            item['Price'] = item['Price'] + 3400
        if roll == 54:
            item['Name'] = "Dust of disappearance"
            item['Aura'] = "Moderate illusion"
            item['Slot'] = "None"
            item['CL'] = 7
            item['Weight'] = 0
            item['Price'] = item['Price'] + 3500
        if roll == 55:
            item['Name'] = "Lens of detection"
            item['Aura'] = md
            item['Slot'] = "Eyes"
            item['CL'] = 9
            item['Weight'] = 1
            item['Price'] = item['Price'] + 3500
        if roll == 56:
            item['Name'] = "Vestment, druid’s"
            item['Aura'] = mt
            item['Slot'] = "Body"
            item['CL'] = 10
            item['Weight'] = 0
            item['Price'] = item['Price'] + 3750
        if roll == 57:
            item['Name'] = "Figurine of wondrous power, silver raven"
            item['Aura'] = "Moderate enchantment and transmutation"
            item['Slot'] = "None"
            item['CL'] = 6
            item['Weight'] = 1
            item['Price'] = item['Price'] + 3800
        if roll == 58:
            item['Name'] = "Amulet of mighty fists +1"
            item['Aura'] = fe
            item['Slot'] = "Neck"
            item['CL'] = 5
            item['Weight'] = 0
            item['Price'] = item['Price'] + 4000
        if roll == 59:
            item['Name'] = "Belt of giant strength +2"
            item['Aura'] = "Moderate transmutation"
            item['Slot'] = "Belt"
            item['CL'] = 8
            item['Weight'] = 1
            item['Price'] = item['Price'] + 4000
        if roll == 60:
            item['Name'] = "Belt of incredible dexterity +2"
            item['Aura'] = "Moderate transmutation"
            item['Slot'] = "Belt"
            item['CL'] = 8
            item['Weight'] = 1
            item['Price'] = item['Price'] + 4000
        if roll == 61:
            item['Name'] = "Belt of mighty constitution +2"
            item['Aura'] = "Moderate transmutation"
            item['Slot'] = "Belt"
            item['CL'] = 8
            item['Weight'] = 1
            item['Price'] = item['Price'] + 4000
        if roll == 62:
            item['Name'] = "Bracers of armor +2"
            item['Aura'] = "Moderate conjuration"
            item['Slot'] = "None"
            item['CL'] = 7
            item['Weight'] = 1
            item['Price'] = item['Price'] + 4000
        if roll == 63:
            item['Name'] = "Cloak of resistance +2"
            item['Aura'] = "Faint abjuration"
            item['Slot'] = "Shoulders"
            item['CL'] = 5
            item['Weight'] = 1
            item['Price'] = item['Price'] + 4000
        if roll == 64:
            item['Name'] = "Gloves of arrow snaring"
            item['Aura'] = fa
            item['Slot'] = "Hands"
            item['CL'] = 3
            item['Weight'] = 0
            item['Price'] = item['Price'] + 4000
        if roll == 65:
            item['Name'] = "Headband of alluring charisma +2"
            item['Aura'] = mt
            item['Slot'] = "Headband"
            item['CL'] = 8
            item['Weight'] = 1
            item['Price'] = item['Price'] + 4000
        if roll == 66:
            item['Name'] = "Headband of inspired wisdom +2"
            item['Aura'] = mt
            item['Slot'] = "Headband"
            item['CL'] = 8
            item['Weight'] = 1
            item['Price'] = item['Price'] + 4000
        if roll == 67:
            item['Name'] = "Headband of vast intelligence +2"
            item['Aura'] = mt
            item['Slot'] = "Headband"
            item['CL'] = 8
            item['Weight'] = 1
            item['Price'] = item['Price'] + 4000
        if roll == 68:
            item['Name'] = "Ioun stone, clear spindle"
            item['Aura'] = "Strong varies"
            item['Slot'] = "None"
            item['CL'] = 12
            item['Weight'] = 0
            item['Price'] = item['Price'] + 4000
        if roll == 69:
            item['Name'] = "Restorative ointment"
            item['Aura'] = fc
            item['Slot'] = "None"
            item['CL'] = 5
            item['Weight'] = 0.5
            item['Price'] = item['Price'] + 4000
        if roll == 70:
            item['Name'] = "Marvelous pigments"
            item['Aura'] = sc
            item['Slot'] = "None"
            item['CL'] = 15
            item['Weight'] = 0
            item['Price'] = item['Price'] + 4000
        if roll == 71:
            item['Name'] = "Pearl of power, 2nd-level spell"
            item['Aura'] = st
            item['Slot'] = "None"
            item['CL'] = 17
            item['Weight'] = 0
            item['Price'] = item['Price'] + 4000
        if roll == 72:
            item['Name'] = "Stone salve"
            item['Aura'] = "Strong abjuration and transmutation"
            item['Slot'] = "None"
            item['CL'] = 13
            item['Weight'] = 0
            item['Price'] = item['Price'] + 4000
        if roll == 73:
            item['Name'] = "Necklace of fireballs type III"
            item['Aura'] = me
            item['Slot'] = "Neck"
            item['CL'] = 10
            item['Weight'] = 1
            item['Price'] = item['Price'] + 4350
        if roll == 74:
            item['Name'] = "Circlet of persuasion"
            item['Aura'] = "Faint transmutation"
            item['Slot'] = "Head"
            item['CL'] = 5
            item['Weight'] = 0
            item['Price'] = item['Price'] + 4500
        if roll == 75:
            item['Name'] = "Slippers of spider climbing"
            item['Aura'] = ft
            item['Slot'] = "Feet"
            item['CL'] = 4
            item['Weight'] = 0.5
            item['Price'] = item['Price'] + 4800
        if roll == 76:
            item['Name'] = "Incense of meditation"
            item['Aura'] = men
            item['Slot'] = "None"
            item['CL'] = 7
            item['Weight'] = 1
            item['Price'] = item['Price'] + 4900
        if roll == 77:
            item['Name'] = "Bag of holding type II"
            item['Aura'] = "Moderate conjuration"
            item['Slot'] = "None"
            item['CL'] = 9
            item['Weight'] = 25
            item['Price'] = item['Price'] + 5000
        if roll == 78:
            item['Name'] = "Bracers of archery, lesser"
            item['Aura'] = "Faint transmutation"
            item['Slot'] = "Wrists"
            item['CL'] = 4
            item['Weight'] = 1
            item['Price'] = item['Price'] + 5000
        if roll == 79:
            item['Name'] = "Ioun stone, dusty rose prism"
            item['Aura'] = "Strong varies"
            item['Slot'] = "None"
            item['CL'] = 12
            item['Weight'] = 0
            item['Price'] = item['Price'] + 5000
        if roll == 80:
            item['Name'] = "Helm of comprehend languages and read magic"
            item['Aura'] = fd
            item['Slot'] = "Head"
            item['CL'] = 4
            item['Weight'] = 3
            item['Price'] = item['Price'] + 5200
        if roll == 81:
            item['Name'] = "Vest of escape"
            item['Aura'] = "Faint conjuration and tramsutation"
            item['Slot'] = "Chest"
            item['CL'] = 4
            item['Weight'] = 0
            item['Price'] = item['Price'] + 5200
        if roll == 82:
            item['Name'] = "Eversmoking bottle"
            item['Aura'] = ft
            item['Slot'] = "None"
            item['CL'] = 3
            item['Weight'] = 1
            item['Price'] = item['Price'] + 5400
        if roll == 83:
            item['Name'] = "Sustaining spoon"
            item['Aura'] = fc
            item['Slot'] = "None"
            item['CL'] = 5
            item['Weight'] = 0
            item['Price'] = item['Price'] + 5400
        if roll == 84:
            item['Name'] = "Necklace of fireballs type IV"
            item['Aura'] = me
            item['Slot'] = "Neck"
            item['CL'] = 10
            item['Weight'] = 1
            item['Price'] = item['Price'] + 5400
        if roll == 85:
            item['Name'] = "Boots of striding and springing"
            item['Aura'] = "Faint transmutation"
            item['Slot'] = "Feet"
            item['CL'] = 3
            item['Weight'] = 1
            item['Price'] = item['Price'] + 5500
        if roll == 86:
            item['Name'] = "Wind fan"
            item['Aura'] = fe
            item['Slot'] = "None"
            item['CL'] = 5
            item['Weight'] = 0
            item['Price'] = item['Price'] + 5500
        if roll == 87:
            item['Name'] = "Necklace of fireballs type V"
            item['Aura'] = me
            item['Slot'] = "Neck"
            item['CL'] = 10
            item['Weight'] = 1
            item['Price'] = item['Price'] + 5850
        if roll == 88:
            item['Name'] = "Horseshoes of a zephyr"
            item['Aura'] = ft
            item['Slot'] = "None"
            item['CL'] = 3
            item['Weight'] = 4
            item['Price'] = item['Price'] + 6000
        if roll == 89:
            item['Name'] = "Pipes of haunting"
            item['Aura'] = fn
            item['Slot'] = "None"
            item['CL'] = 4
            item['Weight'] = 3
            item['Price'] = item['Price'] + 6000
        if roll == 90:
            item['Name'] = "Gloves of swimming and climbing"
            item['Aura'] = ft
            item['Slot'] = "Hands"
            item['CL'] = 5
            item['Weight'] = 0
            item['Price'] = item['Price'] + 6250
        if roll == 91:
            item['Name'] = "Crown of blasting, minor"
            item['Aura'] = "Moderate evocation"
            item['Slot'] = "Head"
            item['CL'] = 6
            item['Weight'] = 1
            item['Price'] = item['Price'] + 6480
        if roll == 92:
            item['Name'] = "Horn of goodness/evil"
            item['Aura'] = ma
            item['Slot'] = "None"
            item['CL'] = 6
            item['Weight'] = 1
            item['Price'] = item['Price'] + 6500
        if roll == 93:
            item['Name'] = "Robe of useful items"
            item['Aura'] = mt
            item['Slot'] = "Body"
            item['CL'] = 9
            item['Weight'] = 1
            item['Price'] = item['Price'] + 7000
        if roll == 94:
            item['Name'] = "Boat, folding"
            item['Aura'] = "Moderate transmutation"
            item['Slot'] = "None"
            item['CL'] = 6
            item['Weight'] = 4
            item['Price'] = item['Price'] + 7200
        if roll == 95:
            item['Name'] = "Cloak of the manta ray"
            item['Aura'] = "moderate transmutation"
            item['Slot'] = "Shoulders"
            item['CL'] = 9
            item['Weight'] = 1
            item['Price'] = item['Price'] + 7200
        if roll == 96:
            item['Name'] = "Bottle of air"
            item['Aura'] = "Moderate transmutation"
            item['Slot'] = "None"
            item['CL'] = 7
            item['Weight'] = 2
            item['Price'] = item['Price'] + 7250
        if roll == 97:
            item['Name'] = "Bag of holding type III"
            item['Aura'] = "Moderate conjuration"
            item['Slot'] = "None"
            item['CL'] = 9
            item['Weight'] = 35
            item['Price'] = item['Price'] + 7400
        if roll == 98:
            item['Name'] = "Periapt of health"
            item['Aura'] = fc
            item['Slot'] = "Neck"
            item['CL'] = 5
            item['Weight'] = 0
            item['Price'] = item['Price'] + 7400
        if roll == 99:
            item['Name'] = "Boots of levitation"
            item['Aura'] = "Faint transmutation"
            item['Slot'] = "Feet"
            item['CL'] = 3
            item['Weight'] = 1
            item['Price'] = item['Price'] + 7500
        if roll == 100:
            item['Name'] = "Harp of charming"
            item['Aura'] = fen
            item['Slot'] = "None"
            item['CL'] = 5
            item['Weight'] = 5
            item['Price'] = item['Price'] + 7500


    if item['Category'] == "Medium":
        if roll == 1:
            item['Name'] = "Amulet of natural armor +2"
            item['Aura'] = "Faint transmutation"
            item['Slot'] = "Neck"
            item['CL'] = 5
            item['Weight'] = 0 
            item['Price'] = item['Price'] + 8000
        if roll == 2:
            item['Name'] = "Golem manual, flesh"
            item['Aura'] = "tk"
            item['Slot'] = "None"
            item['CL'] = 8
            item['Weight'] = 5
            item['Price'] = item['Price'] + 8000
        if roll == 3:
            item['Name'] = "Hand of glory"
            item['Aura'] = "Faint varied"
            item['Slot'] = "Neck"
            item['CL'] = 5
            item['Weight'] = 2
            item['Price'] = item['Price'] + 8000
        if roll == 4:
            item['Name'] = "Ioun stone, deep red sphere"
            item['Aura'] = "Strong varies"
            item['Slot'] = "None"
            item['CL'] = 12
            item['Weight'] = 0
            item['Price'] = item['Price'] + 8000
        if roll == 5:
            item['Name'] = "Ioun stone, incandescent blue sphere"
            item['Aura'] = "Strong varies"
            item['Slot'] = "None"
            item['CL'] = 12
            item['Weight'] = 0
            item['Price'] = item['Price'] + 8000
        if roll == 6:
            item['Name'] = "Ioun stone, pale blue rhomboid"
            item['Aura'] = "Strong varies"
            item['Slot'] = "None"
            item['CL'] = 12
            item['Weight'] = 0
            item['Price'] = item['Price'] + 8000
        if roll == 7:
            item['Name'] = "Ioun stone, pink and green sphere"
            item['Aura'] = "Strong varies"
            item['Slot'] = "None"
            item['CL'] = 12
            item['Weight'] = 0
            item['Price'] = item['Price'] + 8000
        if roll == 8:
            item['Name'] = "Ioun stone, pink rhomboid"
            item['Aura'] = "Strong varies"
            item['Slot'] = "None"
            item['CL'] = 12
            item['Weight'] = 0
            item['Price'] = item['Price'] + 8000
        if roll == 9:
            item['Name'] = "Ioun stone, scarlet and blue sphere"
            item['Aura'] = "Strong varies"
            item['Slot'] = "None"
            item['CL'] = 12
            item['Weight'] = 0
            item['Price'] = item['Price'] + 8000
        if roll == 10:
            item['Name'] = "Deck of illusions"
            item['Aura'] = "Moderate illusion"
            item['Slot'] = "None"
            item['CL'] = 6
            item['Weight'] = 0.5
            item['Price'] = item['Price'] + 8100
        if roll == 11:
            item['Name'] = "Necklace of fireballs type VI"
            item['Aura'] = me
            item['Slot'] = "Neck"
            item['CL'] = 10
            item['Weight'] = 1
            item['Price'] = item['Price'] + 8100
        if roll == 12:
            item['Name'] = "Candle of invocation"
            item['Aura'] = "Strong conjuration"
            item['Slot'] = "None"
            item['CL'] = 17
            item['Weight'] = 0.5
            item['Price'] = item['Price'] + 8400
        if roll == 13:
            item['Name'] = "Robe of blending"
            item['Aura'] = mt
            item['Slot'] = "Body"
            item['CL'] = 10
            item['Weight'] = 1
            item['Price'] = item['Price'] + 8400
        if roll == 14:
            item['Name'] = "Bag of tricks, rust"
            item['Aura'] = "Faint conjuration"
            item['Slot'] = "None"
            item['CL'] = 5
            item['Weight'] = 0
            item['Price'] = item['Price'] + 8500
        if roll == 15:
            item['Name'] = "Necklace of fireballs type VII"
            item['Aura'] = me
            item['Slot'] = "Neck"
            item['CL'] = 10
            item['Weight'] = 1
            item['Price'] = item['Price'] + 8700
        if roll == 16:
            item['Name'] = "Bracers of armor +3"
            item['Aura'] = "Moderate conjuration"
            item['Slot'] = "None"
            item['CL'] = 7
            item['Weight'] = 1
            item['Price'] = item['Price'] + 9000
        if roll == 17:
            item['Name'] = "Cloak of resistance +3"
            item['Aura'] = "Faint abjuration"
            item['Slot'] = "Shoulders"
            item['CL'] = 5
            item['Weight'] = 1
            item['Price'] = item['Price'] + 9000
        if roll == 18:
            item['Name'] = "Decanter of endless water"
            item['Aura'] = "Moderate transmutation"
            item['Slot'] = "None"
            item['CL'] = 9
            item['Weight'] = 2
            item['Price'] = item['Price'] + 9000
        if roll == 19:
            item['Name'] = "Necklace of adaptation"
            item['Aura'] = mt
            item['Slot'] = "Neck"
            item['CL'] = 7
            item['Weight'] = 1
            item['Price'] = item['Price'] + 9000
        if roll == 20:
            item['Name'] = "Pearl of power, 3rd-level spell"
            item['Aura'] = st
            item['Slot'] = "None"
            item['CL'] = 17
            item['Weight'] = 0
            item['Price'] = item['Price'] + 9000
        if roll == 21:
            item['Name'] = "Figurine of wondrous power, serpentine owl"
            item['Aura'] = mt
            item['Slot'] = "None"
            item['CL'] = 11
            item['Weight'] = 1
            item['Price'] = item['Price'] + 9100
        if roll == 22:
            item['Name'] = "Strand of prayer beads, lesser"
            item['Aura'] = "Varies"
            item['Slot'] = "None"
            item['CL'] = "Varies"
            item['Weight'] = 0.5
            item['Price'] = item['Price'] + 9600
        if roll == 23:
            item['Name'] = "Bag of holding type IV"
            item['Aura'] = "Moderate conjuration"
            item['Slot'] = "None"
            item['CL'] = 9
            item['Weight'] = 60
            item['Price'] = item['Price'] + 10000
        if roll == 24:
            item['Name'] = "Belt of physical might +2"
            item['Aura'] = "Strong transmutation"
            item['Slot'] = "Belt"
            item['CL'] = 12
            item['Weight'] = 1
            item['Price'] = item['Price'] + 10000
        if roll == 25:
            item['Name'] = "Figurine of wondrous power, bronze griffon"
            item['Aura'] = mt
            item['Slot'] = "None"
            item['CL'] = 11
            item['Weight'] = 1
            item['Price'] = item['Price'] + 10000
        if roll == 26:
            item['Name'] = "Figurine of wondrous power, ebony fly"
            item['Aura'] = mt
            item['Slot'] = "None"
            item['CL'] = 11
            item['Weight'] = 1
            item['Price'] = item['Price'] + 10000
        if roll == 27:
            item['Name'] = "Glove of storing"
            item['Aura'] = mt
            item['Slot'] = "Hands"
            item['CL'] = 6
            item['Weight'] = 0
            item['Price'] = item['Price'] + 10000
        if roll == 28:
            item['Name'] = "Headband of mental prowess +2"
            item['Aura'] = st
            item['Slot'] = "Headband"
            item['CL'] = 12
            item['Weight'] = 1
            item['Price'] = item['Price'] + 10000
        if roll == 29:
            item['Name'] = "Ioun stone, dark blue rhomboid"
            item['Aura'] = "Strong varies"
            item['Slot'] = "None"
            item['CL'] = 12
            item['Weight'] = 0
            item['Price'] = item['Price'] + 10000
        if roll == 30:
            item['Name'] = "Cape of the mountebank"
            item['Aura'] = "Moderate conjuration"
            item['Slot'] = "Shoulders"
            item['CL'] = 9
            item['Weight'] = 1
            item['Price'] = item['Price'] + 10800
        if roll == 31:
            item['Name'] = "Phylactery of negative channeling"
            item['Aura'] = "Moderate necromancy [evil]"
            item['Slot'] = "Headband"
            item['CL'] = 10
            item['Weight'] = 0
            item['Price'] = item['Price'] + 11000
        if roll == 32:
            item['Name'] = "Phylactery of positive channeling"
            item['Aura'] = "Moderate necromancy [good]"
            item['Slot'] = "Headband"
            item['CL'] = 10
            item['Weight'] = 0
            item['Price'] = item['Price'] + 11000
        if roll == 33:
            item['Name'] = "Gauntlet of rust"
            item['Aura'] = mt
            item['Slot'] = "Hands"
            item['CL'] = 7
            item['Weight'] = 2
            item['Price'] = item['Price'] + 11500
        if roll == 34:
            item['Name'] = "Boots of speed"
            item['Aura'] = "Moderate transmustation"
            item['Slot'] = "Feet"
            item['CL'] = 10
            item['Weight'] = 1
            item['Price'] = item['Price'] + 12000
        if roll == 35:
            item['Name'] = "Goggles of night"
            item['Aura'] = ft
            item['Slot'] = "Eyes"
            item['CL'] = 3
            item['Weight'] = 0
            item['Price'] = item['Price'] + 12000
        if roll == 36:
            item['Name'] = "Golem manual, clay"
            item['Aura'] = "Moderate conjuration, divination, enchantment and transmutation"
            item['Slot'] = "None"
            item['CL'] = 11
            item['Weight'] = 5
            item['Price'] = item['Price'] + 12000
        if roll == 37:
            item['Name'] = "Medallion of thoughts"
            item['Aura'] = fd
            item['Slot'] = "Neck"
            item['CL'] = 5
            item['Weight'] = 0
            item['Price'] = item['Price'] + 12000
        if roll == 38:
            item['Name'] = "Blessed book"
            item['Aura'] = "Moderate transmutation"
            item['Slot'] = "None"
            item['CL'] = 7
            item['Weight'] = 1
            item['Price'] = item['Price'] + 12500
        if roll == 39:
            item['Name'] = "Gem of brightness"
            item['Aura'] = me
            item['Slot'] = "None"
            item['CL'] = 6
            item['Weight'] = 0
            item['Price'] = item['Price'] + 13000
        if roll == 40:
            item['Name'] = "Lyre of building"
            item['Aura'] = mt
            item['Slot'] = "None"
            item['CL'] = 6
            item['Weight'] = 5
            item['Price'] = item['Price'] + 13000
        if roll == 41:
            item['Name'] = "Robe, Monk’s"
            item['Aura'] = mt
            item['Slot'] = "Body"
            item['CL'] = 10
            item['Weight'] = 1
            item['Price'] = item['Price'] + 13000
        if roll == 42:
            item['Name'] = "Cloak of arachnida"
            item['Aura'] = "Moderate conjuration and transmutation"
            item['Slot'] = "Shoulders"
            item['CL'] = 6
            item['Weight'] = 1
            item['Price'] = item['Price'] + 14000
        if roll == 43:
            item['Name'] = "Belt of dwarvenkind"
            item['Aura'] = "Strong divination"
            item['Slot'] = "Belt"
            item['CL'] = 12
            item['Weight'] = 1
            item['Price'] = item['Price'] + 14900
        if roll == 44:
            item['Name'] = "Periapt of wound closure"
            item['Aura'] = mc
            item['Slot'] = "Neck"
            item['CL'] = 10
            item['Weight'] = 0
            item['Price'] = item['Price'] + 15000
        if roll == 45:
            item['Name'] = "Pearl of the sirines"
            item['Aura'] = "Modurate abjuration and transmutation"
            item['Slot'] = "None"
            item['CL'] = 8
            item['Weight'] = 0
            item['Price'] = item['Price'] + 15300
        if roll == 46:
            item['Name'] = "Figurine of wondrous power, onyx dog"
            item['Aura'] = mt
            item['Slot'] = "None"
            item['CL'] = 11
            item['Weight'] = 1
            item['Price'] = item['Price'] + 15500
        if roll == 47:
            item['Name'] = "Amulet of mighty fists +2"
            item['Aura'] = fe
            item['Slot'] = "Neck"
            item['CL'] = 5
            item['Weight'] = 0
            item['Price'] = item['Price'] + 16000
        if roll == 48:
            item['Name'] = "Bag of tricks, tan"
            item['Aura'] = "Moderate conjuration"
            item['Slot'] = "None"
            item['CL'] = 9
            item['Weight'] = 0
            item['Price'] = item['Price'] + 16000
        if roll == 49:
            item['Name'] = "Belt of giant strength +4"
            item['Aura'] = "Moderate transmutation"
            item['Slot'] = "Belt"
            item['CL'] = 8
            item['Weight'] = 1
            item['Price'] = item['Price'] + 16000
        if roll == 50:
            item['Name'] = "Belt of incredible dexterity +4"
            item['Aura'] = "Moderate transmutation"
            item['Slot'] = "Belt"
            item['CL'] = 8
            item['Weight'] = 1
            item['Price'] = item['Price'] + 16000
        if roll == 51:
            item['Name'] = "Belt of might constitution +4"
            item['Aura'] = "Moderate transmutation"
            item['Slot'] = "Belt"
            item['CL'] = 8
            item['Weight'] = 1
            item['Price'] = item['Price'] + 16000
        if roll == 52:
            item['Name'] = "Belt of physical perfection +2"
            item['Aura'] = "Strong transmutation"
            item['Slot'] = "Belt"
            item['CL'] = 16
            item['Weight'] = 1
            item['Price'] = item['Price'] + 16000
        if roll == 53:
            item['Name'] = "Boots, winged"
            item['Aura'] = "Moderate transmutation"
            item['Slot'] = "Feet"
            item['CL'] = 8
            item['Weight'] = 1
            item['Price'] = item['Price'] + 16000
        if roll == 54:
            item['Name'] = "Bracers of armor +4"
            item['Aura'] = "Moderate conjuration"
            item['Slot'] = "None"
            item['CL'] = 7
            item['Weight'] = 1
            item['Price'] = item['Price'] + 16000
        if roll == 55:
            item['Name'] = "Cloak of resistance +4"
            item['Aura'] = "Faint abjuration"
            item['Slot'] = "Shoulders"
            item['CL'] = 5
            item['Weight'] = 1
            item['Price'] = item['Price'] + 16000
        if roll == 56:
            item['Name'] = "Headband of alluring charisma +4"
            item['Aura'] = mt
            item['Slot'] = "Headband"
            item['CL'] = 8
            item['Weight'] = 1
            item['Price'] = item['Price'] + 16000
        if roll == 57:
            item['Name'] = "Headband of inspired wisdom +4"
            item['Aura'] = mt
            item['Slot'] = "Headband"
            item['CL'] = 8
            item['Weight'] = 1
            item['Price'] = item['Price'] + 16000
        if roll == 58:
            item['Name'] = "Headband of mental superiority +2"
            item['Aura'] = st
            item['Slot'] = "Headband"
            item['CL'] = 16
            item['Weight'] = 1
            item['Price'] = item['Price'] + 16000
        if roll == 59:
            item['Name'] = "Headband of vast intelligence +4"
            item['Aura'] = mt
            item['Slot'] = "Headband"
            item['CL'] = 8
            item['Weight'] = 1
            item['Price'] = item['Price'] + 16000
        if roll == 60:
            item['Name'] = "Pearl of power, 4th-level spell"
            item['Aura'] = st
            item['Slot'] = "None"
            item['CL'] = 17
            item['Weight'] = 0
            item['Price'] = item['Price'] + 16000
        if roll == 61:
            item['Name'] = "Scabbard of keen edges"
            item['Aura'] = ft
            item['Slot'] = "None"
            item['CL'] = 5
            item['Weight'] = 1
            item['Price'] = item['Price'] + 16000
        if roll == 62:
            item['Name'] = "Figurine of wondrous power, golden lions"
            item['Aura'] = mt
            item['Slot'] = "None"
            item['CL'] = 11
            item['Weight'] = 1
            item['Price'] = item['Price'] + 16500
        if roll == 63:
            item['Name'] = "Chime of interruption"
            item['Aura'] = "Moderate evocation"
            item['Slot'] = "None"
            item['CL'] = 11
            item['Weight'] = 1
            item['Price'] = item['Price'] + 16800
        if roll == 64:
            item['Name'] = "Broom of flying"
            item['Aura'] = "Moderate transmutation"
            item['Slot'] = "None"
            item['CL'] = 9
            item['Weight'] = 3
            item['Price'] = item['Price'] + 17000
        if roll == 65:
            item['Name'] = "Figurine of wondrous power, marble elephant"
            item['Aura'] = mt
            item['Slot'] = "None"
            item['CL'] = 11
            item['Weight'] = 1
            item['Price'] = item['Price'] + 17000
        if roll == 66:
            item['Name'] = "Amulet of natural armor +3"
            item['Aura'] = "Faint transmutation"
            item['Slot'] = "Neck"
            item['CL'] = 5
            item['Weight'] = 0
            item['Price'] = item['Price'] + 18000
        if roll == 67:
            item['Name'] = "Ioun stone, iridescent spindle"
            item['Aura'] = "Strong varies"
            item['Slot'] = "None"
            item['CL'] = 12
            item['Weight'] = 0
            item['Price'] = item['Price'] + 18000
        if roll == 68:
            item['Name'] = "Bracelet of friends"
            item['Aura'] = "Strong conjuration"
            item['Slot'] = "Wrist"
            item['CL'] = 15
            item['Weight'] = 0
            item['Price'] = item['Price'] + 19000
        if roll == 69:
            item['Name'] = "Carpet of flying, 5 ft. by 5 ft."
            item['Aura'] = "Moderate transmutation"
            item['Slot'] = "None"
            item['CL'] = 10
            item['Weight'] = 8
            item['Price'] = item['Price'] + 20000
        if roll == 70:
            item['Name'] = "Horn of blasting"
            item['Aura'] = me
            item['Slot'] = "None"
            item['CL'] = 7
            item['Weight'] = 1
            item['Price'] = item['Price'] + 20000
        if roll == 71:
            item['Name'] = "Ioun stone, pale lavender ellipsoid"
            item['Aura'] = "Strong varies"
            item['Slot'] = "None"
            item['CL'] = 12
            item['Weight'] = 0
            item['Price'] = item['Price'] + 20000
        if roll == 72:
            item['Name'] = "Ioun stone, pearly white spindle"
            item['Aura'] = "Strong varies"
            item['Slot'] = "None"
            item['CL'] = 12
            item['Weight'] = 0
            item['Price'] = item['Price'] + 20000
        if roll == 73:
            item['Name'] = "Portable hole"
            item['Aura'] = sc
            item['Slot'] = "None"
            item['CL'] = 12
            item['Weight'] = 0
            item['Price'] = item['Price'] + 20000
        if roll == 74:
            item['Name'] = "Stone of good luck (luckstone)"
            item['Aura'] = fe
            item['Slot'] = "None"
            item['CL'] = 5
            item['Weight'] = 0
            item['Price'] = item['Price'] + 20000
        if roll == 75:
            item['Name'] = "Figurine of wondrous power, ivory goats"
            item['Aura'] = mt
            item['Slot'] = "None"
            item['CL'] = 11
            item['Weight'] = 1
            item['Price'] = item['Price'] + 21000
        if roll == 76:
            item['Name'] = "Rope of entanglement"
            item['Aura'] = st
            item['Slot'] = "None"
            item['CL'] = 12
            item['Weight'] = 5
            item['Price'] = item['Price'] + 21000
        if roll == 77:
            item['Name'] = "Golem manual, stone"
            item['Aura'] = "Strong abjuration and enchantment"
            item['Slot'] = "None"
            item['CL'] = 14
            item['Weight'] = 5
            item['Price'] = item['Price'] + 22000
        if roll == 78:
            item['Name'] = "Mask of the skull"
            item['Aura'] = "Strong nacromancy and transmutation"
            item['Slot'] = "Head"
            item['CL'] = 13
            item['Weight'] = 3
            item['Price'] = item['Price'] + 22000
        if roll == 79:
            item['Name'] = "Mattock of the titans"
            item['Aura'] = st
            item['Slot'] = "None"
            item['CL'] = 16
            item['Weight'] = 120
            item['Price'] = item['Price'] + 23348
        if roll == 80:
            item['Name'] = "Crown of blasting, major"
            item['Aura'] = "Strong evocation"
            item['Slot'] = "Head"
            item['CL'] = 17
            item['Weight'] = 1
            item['Price'] = item['Price'] + 23760
        if roll == 81:
            item['Name'] = "Cloak of displacement, minor"
            item['Aura'] = "Faint illusion"
            item['Slot'] = "Shoulders"
            item['CL'] = 3
            item['Weight'] = 1 
            item['Price'] = item['Price'] + 24000
        if roll == 82:
            item['Name'] = "Helm of underwater action"
            item['Aura'] = ft
            item['Slot'] = "Head"
            item['CL'] = 5
            item['Weight'] = 3
            item['Price'] = item['Price'] + 24000
        if roll == 83:
            item['Name'] = "Bracers of archery, greater"
            item['Aura'] = "Moderate transmutation"
            item['Slot'] = "Wrists"
            item['CL'] = 8
            item['Weight'] = 1
            item['Price'] = item['Price'] + 25000
        if roll == 84:
            item['Name'] = "Bracers of armor +5"
            item['Aura'] = "Moderate conjuration"
            item['Slot'] = "None"
            item['CL'] = 7
            item['Weight'] = 1
            item['Price'] = item['Price'] + 25000
        if roll == 85:
            item['Name'] = "Cloak of resistance +5"
            item['Aura'] = "Faint abjuration"
            item['Slot'] = "Shoulders"
            item['CL'] = 5
            item['Weight'] = 1
            item['Price'] = item['Price'] + 25000
        if roll == 86:
            item['Name'] = "Eyes of doom"
            item['Aura'] = mn
            item['Slot'] = "Eyes"
            item['CL'] = 11
            item['Weight'] = 0
            item['Price'] = item['Price'] + 25000
        if roll == 87:
            item['Name'] = "Pearl of power, 5th-level spell"
            item['Aura'] = st
            item['Slot'] = "None"
            item['CL'] = 17
            item['Weight'] = 0
            item['Price'] = item['Price'] + 25000
        if roll == 88:
            item['Name'] = "Maul of the titans"
            item['Aura'] = se
            item['Slot'] = "None"
            item['CL'] = 15
            item['Weight'] = 160
            item['Price'] = item['Price'] + 25305
        if roll == 89:
            item['Name'] = "Cloak of the bat"
            item['Aura'] = "Moderate transmutation"
            item['Slot'] = "Shoulders"
            item['CL'] = 7
            item['Weight'] = 1
            item['Price'] = item['Price'] + 26000
        if roll == 90:
            item['Name'] = "Iron bands of binding"
            item['Aura'] = se
            item['Slot'] = "None"
            item['CL'] = 13
            item['Weight'] = 1
            item['Price'] = item['Price'] + 26000
        if roll == 91:
            item['Name'] = "Cube of frost resistance"
            item['Aura'] = "Faint abjuration"
            item['Slot'] = "None"
            item['CL'] = 5
            item['Weight'] = 2
            item['Price'] = item['Price'] + 27000
        if roll == 92:
            item['Name'] = "Helm of telepathy"
            item['Aura'] = "Faint divination and enchantment"
            item['Slot'] = "Head"
            item['CL'] = 5
            item['Weight'] = 3
            item['Price'] = item['Price'] + 27000
        if roll == 93:
            item['Name'] = "Periapt of proof against poison"
            item['Aura'] = fc
            item['Slot'] = "Neck"
            item['CL'] = 5
            item['Weight'] = 0
            item['Price'] = item['Price'] + 27000
        if roll == 94:
            item['Name'] = "Robe of scintillating colors"
            item['Aura'] = mi
            item['Slot'] = "Body"
            item['CL'] = 11
            item['Weight'] = 1
            item['Price'] = item['Price'] + 27000
        if roll == 95:
            item['Name'] = "Manual of bodily health +1"
            item['Aura'] = se
            item['Slot'] = "None"
            item['CL'] = 17
            item['Weight'] = 5
            item['Price'] = item['Price'] + 27500
        if roll == 96:
            item['Name'] = "Manual of gainful exercise +1"
            item['Aura'] = se
            item['Slot'] = "None"
            item['CL'] = 17
            item['Weight'] = 5
            item['Price'] = item['Price'] + 27500
        if roll == 97:
            item['Name'] = "Manual of quickness in action +1"
            item['Aura'] = se
            item['Slot'] = "None"
            item['CL'] = 17
            item['Weight'] = 5
            item['Price'] = item['Price'] + 27500
        if roll == 98:
            item['Name'] = "Tome of clear thought +1"
            item['Aura'] = se
            item['Slot'] = "None"
            item['CL'] = 17
            item['Weight'] = 5
            item['Price'] = item['Price'] + 27500
        if roll == 99:
            item['Name'] = "Tome of leadership and influence +1"
            item['Aura'] = se
            item['Slot'] = "None"
            item['CL'] = 17
            item['Weight'] = 5
            item['Price'] = item['Price'] + 27500
        if roll == 100:
            item['Name'] = "Tome of understanding +1"
            item['Aura'] = se
            item['Slot'] = "None"
            item['CL'] = 17
            item['Weight'] = 5
            item['Price'] = item['Price'] + 27500

    if item['Category'] == "Major":
        if roll == 1:
            item['Name'] = "Dimensional shackles"
            item['Aura'] = "Moderate abjuration"
            item['Slot'] = "Wrists"
            item['CL'] = 11
            item['Weight'] = 5
            item['Price'] = item['Price'] + 28000
        if roll == 2:
            item['Name'] = "Figurine of wondrous power, obsidian steed"
            item['Aura'] = sc
            item['Slot'] = "None"
            item['CL'] = 15
            item['Weight'] = 1
            item['Price'] = item['Price'] + 28500
        if roll == 3:
            item['Name'] = "Drums of panic"
            item['Aura'] = "Moderate necromancy"
            item['Slot'] = "None"
            item['CL'] = 7
            item['Weight'] = 10
            item['Price'] = item['Price'] + 30000
        if roll == 4:
            item['Name'] = "Ioun stone, orange prism"
            item['Aura'] = "Strong varies"
            item['Slot'] = "None"
            item['CL'] = 12
            item['Weight'] = 0
            item['Price'] = item['Price'] + 30000
        if roll == 5:
            item['Name'] = "Ioun stone, pale green prism"
            item['Aura'] = "Strong varies"
            item['Slot'] = "None"
            item['CL'] = 12
            item['Weight'] = 0
            item['Price'] = item['Price'] + 30000
        if roll == 6:
            item['Name'] = "Lantern of revealing"
            item['Aura'] = fe
            item['Slot'] = "None"
            item['CL'] = 5
            item['Weight'] = 2
            item['Price'] = item['Price'] + 30000
        if roll == 7:
            item['Name'] = "Amulet of natural armor +4"
            item['Aura'] = "Faint transmutation"
            item['Slot'] = "Neck"
            item['CL'] = 5
            item['Weight'] = 0
            item['Price'] = item['Price'] + 32000
        if roll == 8:
            item['Name'] = "Amulet of proof against detection and location"
            item['Aura'] = "Moderate abjuration"
            item['Slot'] = "Neck"
            item['CL'] = 8
            item['Weight'] = 0
            item['Price'] = item['Price'] + 35000
        if roll == 9:
            item['Name'] = "Carpet of flying, 5 ft. by 10 ft."
            item['Aura'] = "Moderate transmutation"
            item['Slot'] = "None"
            item['CL'] = 10
            item['Weight'] = 10
            item['Price'] = item['Price'] + 35000
        if roll == 10:
            item['Name'] = "Golem manual, iron"
            item['Aura'] = "tk"
            item['Slot'] = "None"
            item['CL'] = 16
            item['Weight'] = 5
            item['Price'] = item['Price'] + 36000
        if roll == 11:
            item['Name'] = "Amulet of mighty fists +3"
            item['Aura'] = fe
            item['Slot'] = "Neck"
            item['CL'] = 5
            item['Weight'] = 0
            item['Price'] = item['Price'] + 36000
        if roll == 12:
            item['Name'] = "Belt of giant strength +6"
            item['Aura'] = "Moderate transmutation"
            item['Slot'] = "Belt"
            item['CL'] = 8
            item['Weight'] = 1
            item['Price'] = item['Price'] + 36000
        if roll == 13:
            item['Name'] = "Belt of incredible dexterity +6"
            item['Aura'] = "Moderate transmutation"
            item['Slot'] = "Belt"
            item['CL'] = 8
            item['Weight'] = 1 
            item['Price'] = item['Price'] + 36000
        if roll == 14:
            item['Name'] = "Belt of mighty constitution +6"
            item['Aura'] = "Moderate transmutation"
            item['Slot'] = "Belt"
            item['CL'] = 8
            item['Weight'] = 1
            item['Price'] = item['Price'] + 36000
        if roll == 15:
            item['Name'] = "Bracers of armor +6"
            item['Aura'] = "Moderate conjuration"
            item['Slot'] = "None"
            item['CL'] = 7
            item['Weight'] = 1
            item['Price'] = item['Price'] + 36000
        if roll == 16:
            item['Name'] = "Headband of alluring charisma +6"
            item['Aura'] = mt
            item['Slot'] = "Headband"
            item['CL'] = 8
            item['Weight'] = 1
            item['Price'] = item['Price'] + 36000
        if roll == 17:
            item['Name'] = "Headband of inspired wisdom +6"
            item['Aura'] = mt
            item['Slot'] = "Headband"
            item['CL'] = 8
            item['Weight'] = 1
            item['Price'] = item['Price'] + 36000
        if roll == 18:
            item['Name'] = "Headband of vast intelligence +6"
            item['Aura'] = mt
            item['Slot'] = "Headband"
            item['CL'] = 8
            item['Weight'] = 1
            item['Price'] = item['Price'] + 36000
        if roll == 19:
            item['Name'] = "Ioun stone, vibrant purple prism"
            item['Aura'] = "Strong varies"
            item['Slot'] = "None"
            item['CL'] = 12
            item['Weight'] = 0
            item['Price'] = item['Price'] + 36000
        if roll == 20:
            item['Name'] = "Pearl of power, 6th-level spell"
            item['Aura'] = st
            item['Slot'] = "None"
            item['CL'] = 17
            item['Weight'] = 0
            item['Price'] = item['Price'] + 38000
        if roll == 21:
            item['Name'] = "Scarab of protection"
            item['Aura'] = "Strong abjuration and necromancy"
            item['Slot'] = "Neck"
            item['CL'] = 18
            item['Weight'] = 0
            item['Price'] = item['Price'] + 40000
        if roll == 22:
            item['Name'] = "Belt of physical might +4"
            item['Aura'] = "Strong transmutation"
            item['Slot'] = "Belt"
            item['CL'] = 12
            item['Weight'] = 1
            item['Price'] = item['Price'] + 40000
        if roll == 23:
            item['Name'] = "Headband of mental prowess +4"
            item['Aura'] = st
            item['Slot'] = "Headband"
            item['CL'] = 12
            item['Weight'] = 1
            item['Price'] = item['Price'] + 40000
        if roll == 24:
            item['Name'] = "Ioun stone, lavender and green ellipsoid"
            item['Aura'] = "Strong varies"
            item['Slot'] = "None"
            item['CL'] = 12
            item['Weight'] = 0
            item['Price'] = item['Price'] + 40000
        if roll == 25:
            item['Name'] = "Ring gates"
            item['Aura'] = sc
            item['Slot'] = "None"
            item['CL'] = 17
            item['Weight'] = 1
            item['Price'] = item['Price'] + 42000
        if roll == 26:
            item['Name'] = "Crystal ball"
            item['Aura'] = "Moderate divination"
            item['Slot'] = "None"
            item['CL'] = 10
            item['Weight'] = 7
            item['Price'] = item['Price'] + 44000
        if roll == 27:
            item['Name'] = "Golem manual, stone guardian"
            item['Aura'] = "tk"
            item['Slot'] = "None"
            item['CL'] = 16
            item['Weight'] = 5
            item['Price'] = item['Price'] + 45800
        if roll == 28:
            item['Name'] = "Strand of prayer beads"
            item['Aura'] = "Varies"
            item['Slot'] = "None"
            item['CL'] = "Varies"
            item['Weight'] = 0.5
            item['Price'] = item['Price'] + 48000
        if roll == 29:
            item['Name'] = "Orb of storms"
            item['Aura'] = "Strong varied"
            item['Slot'] = "None"
            item['CL'] = 18
            item['Weight'] = 6
            item['Price'] = item['Price'] + 49000
        if roll == 30:
            item['Name'] = "Boots of teleportation"
            item['Aura'] = "Faint abjuration and transmutation"
            item['Slot'] = "Feet"
            item['CL'] = 5
            item['Weight'] = 1
            item['Price'] = item['Price'] + 49000
        if roll == 31:
            item['Name'] = "Bracers of armor +7"
            item['Aura'] = "Moderate conjuration"
            item['Slot'] = "None"
            item['CL'] = 7
            item['Weight'] = 1
            item['Price'] = item['Price'] + 49000
        if roll == 32:
            item['Name'] = "Pearl of power, 7th-level spell"
            item['Aura'] = st
            item['Slot'] = "None"
            item['CL'] = 17
            item['Weight'] = 0
            item['Price'] = item['Price'] + 50000
        if roll == 33:
            item['Name'] = "Amulet of natural armor +5"
            item['Aura'] = "Faint transmutation"
            item['Slot'] = "Neck"
            item['CL'] = 5
            item['Weight'] = 0
            item['Price'] = item['Price'] + 50000
        if roll == 34:
            item['Name'] = "Cloak of displacement, major"
            item['Aura'] = "Moderate illusion"
            item['Slot'] = "Shoulders"
            item['CL'] = 7
            item['Weight'] = 1
            item['Price'] = item['Price'] + 50000
        if roll == 35:
            item['Name'] = "Crystal ball with see invisibility"
            item['Aura'] = md
            item['Slot'] = "None"
            item['CL'] = 10
            item['Weight'] = 7
            item['Price'] = item['Price'] + 50000
        if roll == 36:
            item['Name'] = "Horn of Valhalla"
            item['Aura'] = sc
            item['Slot'] = "None"
            item['CL'] = 13
            item['Weight'] = 2
            item['Price'] = item['Price'] + 51000
        if roll == 37:
            item['Name'] = "Crystal ball with detect thoughts"
            item['Aura'] = md
            item['Slot'] = "None"
            item['CL'] = 10
            item['Weight'] = 7
            item['Price'] = item['Price'] + 54000
        if roll == 38:
            item['Name'] = "Wings of flying"
            item['Aura'] = mt
            item['Slot'] = "Shoulders"
            item['CL'] = 10
            item['Weight'] = 2
            item['Price'] = item['Price'] + 55000
        if roll == 39:
            item['Name'] = "Cloak of etherealness"
            item['Aura'] = "Strong transmutation"
            item['Slot'] = "None"
            item['CL'] = 15
            item['Weight'] = 1
            item['Price'] = item['Price'] + 55000
        if roll == 40:
            item['Name'] = "Instant fortress"
            item['Aura'] = sc
            item['Slot'] = "None"
            item['CL'] = 13
            item['Weight'] = 1
            item['Price'] = item['Price'] + 55000
        if roll == 41:
            item['Name'] = "Manual of bodily health +2"
            item['Aura'] = se
            item['Slot'] = "None"
            item['CL'] = 17
            item['Weight'] = 5
            item['Price'] = item['Price'] + 55000
        if roll == 42:
            item['Name'] = "Manual of gainful exercise +2"
            item['Aura'] = se
            item['Slot'] = "None"
            item['CL'] = 17
            item['Weight'] = 5
            item['Price'] = item['Price'] + 55000
        if roll == 43:
            item['Name'] = "Manual of quickness in action +2"
            item['Aura'] = se
            item['Slot'] = "None"
            item['CL'] = 17
            item['Weight'] = 5
            item['Price'] = item['Price'] + 55000
        if roll == 44:
            item['Name'] = "Tome of clear thought +2"
            item['Aura'] = se
            item['Slot'] = "None"
            item['CL'] = 17
            item['Weight'] = 5
            item['Price'] = item['Price'] + 55000
        if roll == 45:
            item['Name'] = "Tome of leadership and influence +2"
            item['Aura'] = se
            item['Slot'] = "None"
            item['CL'] = 17
            item['Weight'] = 5
            item['Price'] = item['Price'] + 55000
        if roll == 46:
            item['Name'] = "Tome of understanding +2"
            item['Aura'] = se
            item['Slot'] = "None"
            item['CL'] = 17
            item['Weight'] = 5
            item['Price'] = item['Price'] + 56000
        if roll == 47:
            item['Name'] = "Eyes of charming"
            item['Aura'] = men
            item['Slot'] = "Eyes"
            item['CL'] = 7
            item['Weight'] = 0
            item['Price'] = item['Price'] + 58000
        if roll == 48:
            item['Name'] = "Robe of stars"
            item['Aura'] = "Strong varied"
            item['Slot'] = "Body"
            item['CL'] = 15
            item['Weight'] = 1
            item['Price'] = item['Price'] + 60000
        if roll == 49:
            item['Name'] = "Carpet of flying, 10 ft. by 10 ft."
            item['Aura'] = "Moderate transmutation"
            item['Slot'] = "None"
            item['CL'] = 10
            item['Weight'] = 15
            item['Price'] = item['Price'] + 60000
        if roll == 50:
            item['Name'] = "Darkskull"
            item['Aura'] = "Moderate evocation [evil]"
            item['Slot'] = "None"
            item['CL'] = 9
            item['Weight'] = 5
            item['Price'] = item['Price'] + 62000
        if roll == 51:
            item['Name'] = "Cube of force"
            item['Aura'] = "Moderate evocation"
            item['Slot'] = "None"
            item['CL'] = 10
            item['Weight'] = 0.5
            item['Price'] = item['Price'] + 64000
        if roll == 52:
            item['Name'] = "Amulet of mighty fists +4"
            item['Aura'] = fe
            item['Slot'] = "Neck"
            item['CL'] = 5
            item['Weight'] = 0
            item['Price'] = item['Price'] + 64000
        if roll == 53:
            item['Name'] = "Belt of physical perfection +4"
            item['Aura'] = "Strong transmutation"
            item['Slot'] = "Belt"
            item['CL'] = 16
            item['Weight'] = 1
            item['Price'] = item['Price'] + 64000
        if roll == 54:
            item['Name'] = "Bracers of armor +8"
            item['Aura'] = "Moderate conjuration"
            item['Slot'] = "None"
            item['CL'] = 7
            item['Weight'] = 1
            item['Price'] = item['Price'] + 64000
        if roll == 55:
            item['Name'] = "Headband of mental superiority +4"
            item['Aura'] = st
            item['Slot'] = "Headband"
            item['CL'] = 16
            item['Weight'] = 1
            item['Price'] = item['Price'] + 64000
        if roll == 56:
            item['Name'] = "Pearl of power, 8th-level spell"
            item['Aura'] = st
            item['Slot'] = "None"
            item['CL'] = 17
            item['Weight'] = 0
            item['Price'] = item['Price'] + 64000
        if roll == 57:
            item['Name'] = "Crystal ball with telepathy"
            item['Aura'] = md
            item['Slot'] = "None"
            item['CL'] = 10
            item['Weight'] = 7
            item['Price'] = item['Price'] + 70000
        if roll == 58:
            item['Name'] = "Horn of blasting, greater"
            item['Aura'] = se
            item['Slot'] = "None"
            item['CL'] = 16
            item['Weight'] = 1
            item['Price'] = item['Price'] + 70000
        if roll == 59:
            item['Name'] = "Pearl of power, two spells"
            item['Aura'] = st
            item['Slot'] = "None"
            item['CL'] = 17
            item['Weight'] = 0
            item['Price'] = item['Price'] + 70000
        if roll == 60:
            item['Name'] = "Helm of teleportation"
            item['Aura'] = mc
            item['Slot'] = "Head"
            item['CL'] = 9
            item['Weight'] = 3
            item['Price'] = item['Price'] + 73500
        if roll == 61:
            item['Name'] = "Gem of seeing"
            item['Aura'] = md
            item['Slot'] = "None"
            item['CL'] = 10
            item['Weight'] = 0
            item['Price'] = item['Price'] + 75000
        if roll == 62:
            item['Name'] = "Robe of the archmagi"
            item['Aura'] = "Strong varied"
            item['Slot'] = "Body"
            item['CL'] = 14
            item['Weight'] = 1
            item['Price'] = item['Price'] + 75000
        if roll == 63:
            item['Name'] = "Mantle of faith"
            item['Aura'] = "Strong abjuration [good]"
            item['Slot'] = "Chest"
            item['CL'] = 20
            item['Weight'] = 0
            item['Price'] = item['Price'] + 76000
        if roll == 64:
            item['Name'] = "Crystal ball with true seeing"
            item['Aura'] = md
            item['Slot'] = "None"
            item['CL'] = 10
            item['Weight'] = 7
            item['Price'] = item['Price'] + 80000
        if roll == 65:
            item['Name'] = "Pearl of power, 9th-level spell"
            item['Aura'] = st
            item['Slot'] = "None"
            item['CL'] = 17
            item['Weight'] = 0
            item['Price'] = item['Price'] + 81000
        if roll == 66:
            item['Name'] = "Well of many worlds"
            item['Aura'] = sc
            item['Slot'] = "None"
            item['CL'] = 17
            item['Weight'] = 0
            item['Price'] = item['Price'] + 82000
        if roll == 67:
            item['Name'] = "Manual of bodily health +3"
            item['Aura'] = se
            item['Slot'] = "None"
            item['CL'] = 17
            item['Weight'] = 5
            item['Price'] = item['Price'] + 82500
        if roll == 68:
            item['Name'] = "Manual of gainful exercise +3"
            item['Aura'] = se
            item['Slot'] = "None"
            item['CL'] = 17
            item['Weight'] = 5
            item['Price'] = item['Price'] + 82500
        if roll == 69:
            item['Name'] = "Manual of quickness in action +3"
            item['Aura'] = se
            item['Slot'] = "None"
            item['CL'] = 17
            item['Weight'] = 5
            item['Price'] = item['Price'] + 82500
        if roll == 70:
            item['Name'] = "Tome of clear thought +3"
            item['Aura'] = se
            item['Slot'] = "None"
            item['CL'] = 17
            item['Weight'] = 5
            item['Price'] = item['Price'] + 82500
        if roll == 71:
            item['Name'] = "Tome of leadership and influence +3"
            item['Aura'] = se
            item['Slot'] = "None"
            item['CL'] = 17
            item['Weight'] = 5
            item['Price'] = item['Price'] + 82500
        if roll == 72:
            item['Name'] = "Tome of understanding +3"
            item['Aura'] = se
            item['Slot'] = "None"
            item['CL'] = 17
            item['Weight'] = 5
            item['Price'] = item['Price'] + 82500
        if roll == 73:
            item['Name'] = "Apparatus of the crab"
            item['Aura'] = "Strong evocation and transmutation"
            item['Slot'] = "None" 
            item['CL'] = 9
            item['Weight'] = 500
            item['Price'] = item['Price'] + 90000
        if roll == 74:
            item['Name'] = "Belt of physical might +6"
            item['Aura'] = "Strong transmutation"
            item['Slot'] = "Belt"
            item['CL'] = 12
            item['Weight'] = 1
            item['Price'] = item['Price'] + 90000
        if roll == 75:
            item['Name'] = "Headband of mental prowess +6"
            item['Aura'] = st
            item['Slot'] = "Headband"
            item['CL'] = 12
            item['Weight'] = 1
            item['Price'] = item['Price'] + 90000
        if roll == 76:
            item['Name'] = "Mantle of spell resistance"
            item['Aura'] = ma
            item['Slot'] = "Chest"
            item['CL'] = 9
            item['Weight'] = 0
            item['Price'] = item['Price'] + 90000
        if roll == 77:
            item['Name'] = "Mirror of opposition"
            item['Aura'] = sn
            item['Slot'] = "None"
            item['CL'] = 15
            item['Weight'] = 45
            item['Price'] = item['Price'] + 92000
        if roll == 78:
            item['Name'] = "Strand of prayer beads, greater"
            item['Aura'] = "Varies"
            item['Slot'] = "None"
            item['CL'] = "Varies"
            item['Weight'] = 0.5
            item['Price'] = item['Price'] + 95800
        if roll == 79:
            item['Name'] = "Amulet of mighty fists +5"
            item['Aura'] = fe
            item['Slot'] = "Neck"
            item['CL'] = 5
            item['Weight'] = 0
            item['Price'] = item['Price'] + 100000
        if roll == 80:
            item['Name'] = "Manual of bodily health +4"
            item['Aura'] = se
            item['Slot'] = "None"
            item['CL'] = 17
            item['Weight'] = 5
            item['Price'] = item['Price'] + 110000
        if roll == 81:
            item['Name'] = "Manual of gainful exercise +4"
            item['Aura'] = se
            item['Slot'] = "None"
            item['CL'] = 17
            item['Weight'] = 5
            item['Price'] = item['Price'] + 110000
        if roll == 82:
            item['Name'] = "Manual of quickness in action +4"
            item['Aura'] = se
            item['Slot'] = "None"
            item['CL'] = 17
            item['Weight'] = 5
            item['Price'] = item['Price'] + 110000
        if roll == 83:
            item['Name'] = "Tome of clear thought +4"
            item['Aura'] = se
            item['Slot'] = "None"
            item['CL'] = 17
            item['Weight'] = 5
            item['Price'] = item['Price'] + 110000
        if roll == 84:
            item['Name'] = "Tome of leadership and influence +4"
            item['Aura'] = se
            item['Slot'] = "None"
            item['CL'] = 17
            item['Weight'] = 5
            item['Price'] = item['Price'] + 110000
        if roll == 85:
            item['Name'] = "Tome of understanding +4"
            item['Aura'] = se
            item['Slot'] = "None"
            item['CL'] = 17
            item['Weight'] = 5
            item['Price'] = item['Price'] + 110000
        if roll == 86:
            item['Name'] = "Amulet of the planes"
            item['Aura'] = "Strong conjuration"
            item['Slot'] = "Neck"
            item['CL'] = 15
            item['Weight'] = 0
            item['Price'] = item['Price'] + 120000
        if roll == 87:
            item['Name'] = "Robe of eyes"
            item['Aura'] = md
            item['Slot'] = "None"
            item['CL'] = 11
            item['Weight'] = 1
            item['Price'] = item['Price'] + 120000
        if roll == 88:
            item['Name'] = "Helm of brilliance"
            item['Aura'] ="Strong varied"
            item['Slot'] = "Head"
            item['CL'] = 13
            item['Weight'] = 3
            item['Price'] = item['Price'] + 125000
        if roll == 89:
            item['Name'] = "Manual of bodily health +5"
            item['Aura'] = se
            item['Slot'] = "None"
            item['CL'] = 17
            item['Weight'] = 5
            item['Price'] = item['Price'] + 137500
        if roll == 90:
            item['Name'] = "Manual of gainful exercise +5"
            item['Aura'] = se
            item['Slot'] = "None"
            item['CL'] = 17
            item['Weight'] = 5
            item['Price'] = item['Price'] + 137500
        if roll == 91:
            item['Name'] = "Manual of quickness in action +5"
            item['Aura'] = se
            item['Slot'] = "None"
            item['CL'] = 17
            item['Weight'] = 5
            item['Price'] = item['Price'] + 137500
        if roll == 92:
            item['Name'] = "Tome of clear thought +5"
            item['Aura'] = se
            item['Slot'] = "None"
            item['CL'] = 17
            item['Weight'] = 5
            item['Price'] = item['Price'] + 137500
        if roll == 93:
            item['Name'] = "Tome of leadership and influence +5"
            item['Aura'] = se
            item['Slot'] = "None"
            item['CL'] = 17
            item['Weight'] = 5
            item['Price'] = item['Price'] + 137500
        if roll == 94:
            item['Name'] = "Tome of understanding +5"
            item['Aura'] = se
            item['Slot'] = "None"
            item['CL'] = 17
            item['Weight'] = 5
            item['Price'] = item['Price'] + 137500
        if roll == 95:
            item['Name'] = "Belt of physical perfection +6"
            item['Aura'] = "Strong transmutation"
            item['Slot'] = "Belt"
            item['CL'] = 16
            item['Weight'] = 1
            item['Price'] = item['Price'] + 144000
        if roll == 96:
            item['Name'] = "Headband of mental superiority +6"
            item['Aura'] = st
            item['Slot'] = "Headband"
            item['CL'] = 16
            item['Weight'] = 1
            item['Price'] = item['Price'] + 144000
        if roll == 97:
            item['Name'] = "Efreeti bottle"
            item['Aura'] = sc
            item['Slot'] = "None"
            item['CL'] = 14
            item['Weight'] = 1
            item['Price'] = item['Price'] + 145000
        if roll == 98:
            item['Name'] = "Cubic gate"
            item['Aura'] = "Strong conjuration"
            item['Slot'] = "None"
            item['CL'] = 14
            item['Weight'] = 2
            item['Price'] = item['Price'] + 164000
        if roll == 99:
            item['Name'] = "Iron flask"
            item['Aura'] = sc
            item['Slot'] = "None"
            item['CL'] = 20
            item['Weight'] = 1
            item['Price'] = item['Price'] + 170000
        if roll == 100:
            item['Name'] = "Mirror of life trapping"
            item['Aura'] = sa
            item['Slot'] = "None"
            item['CL'] = 17
            item['Weight'] = 50
            item['Price'] = item['Price'] + 200000

# Weapons
# Page 467
def createWeapon( item ):
    roll = d(100)
    if roll >= 91:
        item['Size'] = "Any"
    elif roll >= 31:
        item['Size'] = "Medium"
    else:
        item['Size'] = "Small"

    roll = d(100)
    if roll <=30:
        item['Shines'] = True
    elif roll <= 45:
        item['Clue?'] = True
    
    level = 0
    levelPrice = [ 2000, 8000, 18000, 32000, 50000, 72000, 98000, 128000, 162000, 200000 ]
    special = 0



    reroll = 1
    while reroll > 0:
        roll = d(100)
        ranged = random.choices( [ True, False ] )
        if item['Category'] == "Minor":
            if 1 <= roll <= 70:
                level = level + 1

            if 71 <= roll <= 85:
                level = level + 2

            if 86 <= roll <= 90:
                # Specyfic armor
                roll = d(100)
                #tk

            if 91 <= roll <= 100:
                # Special ability and roll again                
                print_debug( "rerolling type" )
                reroll = reroll + 1
                special = special + 1

        item['Name'] = "+" + str( level ) + " shield"


        reroll = reroll - 1

    i = 0
    while special > 0 and level < 10:
        i = i + 1
        item['Name'] = item['Name'] + ", "

        roll = d(100)
        if not ranged:
            if 1 <= roll <= 10:
                item['Name'] = item['Name'] +"Bane"
                level = level + 1
            if 11 <= roll <= 17:
                item['Name'] = item['Name'] +"Defending"
                level = level + 1
            if 18 <= roll <= 27:
                item['Name'] = item['Name'] +"Flaming"
                level = level + 1
            if 28 <= roll <= 37:
                item['Name'] = item['Name'] +"Frost"
                level = level + 1
            if 38 <= roll <= 47:
                item['Name'] = item['Name'] +"Shock"
                level = level + 1
            if 48 <= roll <= 56:
                item['Name'] = item['Name'] +"Ghost touch"
                level = level + 1
            if 57 <= roll <= 67:
                item['Name'] = item['Name'] +"Keen"
                level = level + 1
            if 68 <= roll <= 71:
                item['Name'] = item['Name'] +"Ki focus"
                level = level + 1
            if 72 <= roll <= 75:
                item['Name'] = item['Name'] +"Merciful"
                level = level + 1
            if 76 <= roll <= 82:
                item['Name'] = item['Name'] +"Mighty cleaving"
                level = level + 1
            if 83 <= roll <= 87:
                item['Name'] = item['Name'] +"Spell storing"
                level = level + 1
            if 88 <= roll <= 91:
                item['Name'] = item['Name'] +"Throwing"
                level = level + 1
            if 92 <= roll <= 95:
                item['Name'] = item['Name'] +"Thundering"
                level = level + 1
            if 96 <= roll <= 99:
                item['Name'] = item['Name'] +"Vicious"
                level = level + 1
            if roll == 100:
                special = special +2
        elif ranged:
            pass #tk

        special = special -1
        
    if level > 0:
        item['Price'] = item['Price'] + levelPrice[ level ]

# Main program
files = glob.glob( os.path.join( os.path.dirname( __file__ ), '*.csv' ) )
for file in files:
    print( file )

    with open( file, newline='' ) as csvfile_cities:
        reader = csv.DictReader( csvfile_cities )
        for city in reader:
            
            minor_count = 0
            medium_count = 0
            major_count = 0

            # How many items of each category to generate per city
            if( int( city['Population'] ) > 25000 ):
                city['Base Value'] = 16000 # Metropolis
                medium_count = d( 4, 4 )
                major_count = d( 4, 3 )

            elif( int( city['Population'] ) > 10000 ):
                city['Base Value'] = 8000 # Large city
                minor_count = d( 4, 4 )
                medium_count = d( 4, 3 )
                major_count = d( 4, 2 )

            elif( int( city['Population'] ) > 5000 ):
                city['Base Value'] = 4000 # Small city
                minor_count = d( 4, 4 )
                medium_count = d( 4, 3 )
                major_count = d( 6, 1 )

            elif( int( city['Population'] ) > 2000 ):
                city['Base Value'] = 2000 # Large town
                minor_count = d( 4, 3 )
                medium_count = d( 4, 2 )
                major_count = d( 4, 1 )

            elif( int( city['Population'] ) > 200 ):
                city['Base Value'] = 1000 # Small town
                minor_count = d( 4, 3 )
                medium_count = d( 6, 1 )

            elif( int( city['Population'] ) > 60 ):
                city['Base Value'] = 500 # Village
                minor_count = d( 4, 2 )
                medium_count = d( 4, 1 )

            elif( int( city['Population'] ) > 60 ):
                city['Base Value'] = 200 # Hamlet
                minor_count = d( 6, 1 )

            else:
                city['Base Value'] = 50 # Thorp
                minor_count = d( 4, 1 )

            for x in range( minor_count ):
                item = {}
                createItem( item, "Minor" )
                itemList.append( item )

            for x in range( medium_count ):
                item = {}
                createItem( item, "Medium" )
                itemList.append( item )

            for x in range( major_count ):
                item = {}
                createItem( item, "Major" )
                itemList.append( item )

    # Save generated files
    with open( os.path.join( os.path.dirname( file ), 'MagicItems_' + os.path.basename( file ) + ".csv" ), 'w', newline='' ) as csvfile_magicItems:
        fieldnames = [
            "Name",
            "City",
            "Province",
            "Price",
            "Min Price",
            "Category",
            "Type",
            "Special Material",
            "Aura",
            "Slot",
            "Size",
            "Weight",
            "CL",
            "Inteligent?",
            "Clue?"
        ]
        writer = csv.DictWriter( csvfile_magicItems, fieldnames=fieldnames )

        statscount_wonderous = 0
        statscount_inteligent = 0

        writer.writeheader()
        for item in itemList:
            if item['Type'] == "Wonderoud items":
                statscount_wonderous = statscount_wonderous + 1
            if item['Inteligent?'] == True:
                statscount_inteligent = statscount_inteligent + 1
            
            writer.writerow( item )

        print( "Created", len( itemList ), "items" )
        print( "Wonderous items:", statscount_wonderous)
        print( "Inteligent items:", statscount_inteligent)